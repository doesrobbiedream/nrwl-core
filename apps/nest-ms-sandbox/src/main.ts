/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Logger } from '@nestjs/common'
import { NestFactory } from '@nestjs/core'

import { AppModule } from './app/app.module'
import { Transport } from '@nestjs/microservices'
import { MicroserviceExceptionFilter } from '@doesrobbiedream/nest-core'

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.REDIS,
    options: {
      url: process.env.REDIS_URL
    }
  })
  app.useGlobalFilters(new MicroserviceExceptionFilter())
  await app.listen(() => Logger.log('Nest MicroService Sandbox is Ready'))
}

bootstrap()
