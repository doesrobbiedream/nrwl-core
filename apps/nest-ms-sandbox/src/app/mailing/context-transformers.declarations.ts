import { NotificationServerDeclarations } from '@doesrobbiedream/notification-server'
import { ConfigService } from '@nestjs/config'
import path from 'path'
import NotificationContextTransformers = NotificationServerDeclarations.NotificationContextTransformers

export function ContextTransformersDeclarationsFactory(
  config: ConfigService
): NotificationContextTransformers {
  return {
    'welcome-email': (context) => ({
      to: context.user.email,
      subject: "Welcome! Let's activate your account",
      from: 'Nrwl Core Support <support@nrwlcore.com>',
      template: './welcome', // VERY IMPORTANT TO PUT THE ./ BEFORE THE TEMPLATE NAME. IT CRASHES OTHERWISE
      attachments: [
        {
          filename: 'logo.png',
          path: path.join(
            __dirname,
            config.get<string>('MAILER_TEMPLATES_FOLDER'),
            'images/logo.png'
          ),
          cid: 'logo'
        }
      ],
      context: {
        styles: {
          fontLinkSrc:
            'https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,400;1,200&display=swap',
          fontFamily: 'Nunito, sans-serif',
          color1: '#410B13',
          bkg1: '#410B13',
          bkg1hover: '#9D1B2E',
          bkg1contrast: 'white',
          color2: '#473198',
          bkg2: '#473198',
          bkg2hover: '#6E56C8',
          bkg2contrast: 'white'
        },
        header: {
          homepage: 'https://someawesomesite.com',
          logo: 'cid:logo',
          logoAlt: 'Some Awesome Site',
          logoTitle: 'Some Awesome Site'
        },
        welcomeSection: {
          title: `¡Bienvenido ${context.user.first_name}!`,
          subtitle: 'Estamos muy contentos de tenerte con nosotros'
        },
        verificationSection: {
          text: 'Vamos a verificar tu dirección de correo electrónico',
          ctaText: 'Verificar mi dirección de correo',
          link: `${config.get<string>('MAILER_URL')}?token=${context.validation_token}`
        }
      }
    })
  }
}
