import { Module } from '@nestjs/common'
import { AuthenticationServerModule } from '@doesrobbiedream/authentication-server'
import {
  OrchestratorServerConfigs,
  OrchestratorServerModule
} from '@doesrobbiedream/orchestrator-server'
import {
  MessageBrokerModule,
  MessageBrokerProxyService,
  MicroserviceMessagingDeclarations
} from '@doesrobbiedream/nest-core'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { Transport } from '@nestjs/microservices'
import { MailerConfig } from './mailer.configuration'
import { NotificationServerModule } from '@doesrobbiedream/notification-server'
import { ContextTransformersDeclarationsFactory } from './mailing/context-transformers.declarations'
import MESSAGE_BROKER_TOKEN = MicroserviceMessagingDeclarations.MESSAGE_BROKER_TOKEN
import PROXY_PROVIDER = OrchestratorServerConfigs.PROXY_PROVIDER

@Module({
  imports: [
    MessageBrokerModule.forRootAsync({
      inject: [ConfigService],
      factory: (config: ConfigService) =>
        new MessageBrokerProxyService({
          transport: Transport.REDIS,
          options: { url: config.get<string>('REDIS_URL') }
        }),
      imports: [ConfigModule]
    }),
    OrchestratorServerModule.register({
      proxy_provider: {
        provide: PROXY_PROVIDER,
        useExisting: MESSAGE_BROKER_TOKEN
      }
    }),
    AuthenticationServerModule,
    NotificationServerModule.registerAsync({
      mailer_config: MailerConfig,
      imports: [ConfigModule],
      transformersFactory: {
        useFactory: (config: ConfigService) => ContextTransformersDeclarationsFactory(config),
        provide: 'CONTEXT_TRANSFORMER',
        inject: [ConfigService]
      }
    })
  ],
  controllers: [],
  providers: []
})
export class AppModule {}
