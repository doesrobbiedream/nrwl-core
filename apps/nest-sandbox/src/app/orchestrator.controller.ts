import { Body, Controller, Inject, Param, Post } from '@nestjs/common'
import {
  MessageBrokerProxyService,
  MicroserviceMessagingDeclarations
} from '@doesrobbiedream/nest-core'
import MESSAGE_BROKER_TOKEN = MicroserviceMessagingDeclarations.MESSAGE_BROKER_TOKEN

@Controller('/redis-gateway')
export class OrchestratorController {
  constructor(@Inject(MESSAGE_BROKER_TOKEN) protected proxy: MessageBrokerProxyService) {}

  @Post(':pattern')
  redisGateway(@Param('pattern') pattern, @Body() req) {
    return this.proxy.client.send(pattern, req)
  }
}
