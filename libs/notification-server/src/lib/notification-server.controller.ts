import { Body, Controller } from '@nestjs/common'
import { MessagePattern } from '@nestjs/microservices'
import { NotificationServerService } from './notification-server.service'
import { NotificationServerDeclarations } from './notification-server.declarations'
import EmailNotificationInput = NotificationServerDeclarations.EmailNotificationInput

@Controller('notification-server')
export class NotificationServerController {
  constructor(protected service: NotificationServerService) {}

  @MessagePattern('notifications.email:send')
  sendEmail(@Body() mailOptions: EmailNotificationInput) {
    return this.service.sendMail(mailOptions)
  }
}
