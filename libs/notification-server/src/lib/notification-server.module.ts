import _has from 'lodash/has'
import { DynamicModule, Module } from '@nestjs/common'
import { NotificationServerController } from './notification-server.controller'
import { MailerModule, MailerOptions } from '@nestjs-modules/mailer'
import { NotificationServerDeclarations } from './notification-server.declarations'
import { MailerAsyncOptions } from '@nestjs-modules/mailer/dist/interfaces/mailer-async-options.interface'
import { NotificationServerService } from './notification-server.service'
import { EXCEPTION_FILTER_PROVIDER } from '@doesrobbiedream/nest-core'
import NotificationServerConfig = NotificationServerDeclarations.NotificationServerConfig
import NotificationServerConfigAsync = NotificationServerDeclarations.NotificationServerConfigAsync
import NOTIFICATION_TRANSFORMER_TOKEN = NotificationServerDeclarations.NOTIFICATION_TRANSFORMER_TOKEN

@Module({
  controllers: [],
  providers: [NotificationServerService]
})
export class NotificationServerModule {
  public static register(config: NotificationServerConfig): DynamicModule {
    return {
      global: true,
      module: NotificationServerModule,
      imports: [this.MailerModule(config.mailer_config)],
      providers: [
        { provide: NOTIFICATION_TRANSFORMER_TOKEN, useValue: config.transformers },
        EXCEPTION_FILTER_PROVIDER()
      ],
      controllers: [NotificationServerController]
    }
  }

  public static registerAsync(config: NotificationServerConfigAsync) {
    return {
      global: true,
      module: NotificationServerModule,
      imports: [...(config.imports || []), this.MailerModule(config.mailer_config)],
      providers: [
        { ...config.transformersFactory, provide: NOTIFICATION_TRANSFORMER_TOKEN },
        EXCEPTION_FILTER_PROVIDER()
      ],
      controllers: [NotificationServerController]
    }
  }

  protected static MailerModule(config: MailerOptions | MailerAsyncOptions) {
    return _has(config, 'useFactory') || _has(config, 'useClass') || _has(config, 'useExisting')
      ? MailerModule.forRootAsync(config as MailerAsyncOptions)
      : MailerModule.forRoot(config as MailerOptions)
  }
}
