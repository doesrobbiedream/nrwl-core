import { Inject, Injectable } from '@nestjs/common'
import { MailerService } from '@nestjs-modules/mailer'
import { NotificationServerDeclarations } from './notification-server.declarations'
import { NotificationServerExceptions } from './notification-server.exceptions'
import NOTIFICATION_TRANSFORMER_TOKEN = NotificationServerDeclarations.NOTIFICATION_TRANSFORMER_TOKEN
import NotificationContextTransformers = NotificationServerDeclarations.NotificationContextTransformers
import EmailNotificationInput = NotificationServerDeclarations.EmailNotificationInput
import TransformerNotFoundException = NotificationServerExceptions.TransformerNotFoundException
import EmailNotificationFailedException = NotificationServerExceptions.EmailNotificationFailedException

@Injectable()
export class NotificationServerService {
  constructor(
    @Inject(NOTIFICATION_TRANSFORMER_TOKEN) protected transformers: NotificationContextTransformers,
    protected mailer: MailerService
  ) {}

  sendMail(options: EmailNotificationInput) {
    if (!this.transformers[options.transformer]) {
      throw new TransformerNotFoundException()
    }
    try {
      return this.mailer
        .sendMail(this.transformers[options.transformer](options.data))
        .catch((e) => {
          throw new EmailNotificationFailedException({}, e)
        })
    } catch (e) {
      throw new EmailNotificationFailedException({}, e)
    }
  }
}
