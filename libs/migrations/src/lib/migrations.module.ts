import { Module } from '@nestjs/common'
import { ConsoleModule } from '@doesrobbiedream/console'

@Module({
  imports: [ConsoleModule],
  providers: [],
  exports: []
})
export class MyModule {}
