import { MyModule } from './migrations.module'
import { BootstrapConsole } from '@doesrobbiedream/console'

export function initMigrationsConsole() {
  const bootstrap = new BootstrapConsole({
    module: MyModule,
    useDecorators: true
  })
  bootstrap.init().then(async (app) => {
    try {
      await app.init()
      await bootstrap.boot()
      process.exit(0)
    } catch (e) {
      console.error('Error', e)
      process.exit(1)
    }
  })
}
