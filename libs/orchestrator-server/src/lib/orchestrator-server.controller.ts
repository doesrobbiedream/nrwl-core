import { Controller, Inject } from '@nestjs/common'
import { CONTEXT, MessagePattern, RequestContext } from '@nestjs/microservices'
import { OrchestratorServerService } from './orchestrator-server.service'
import { OrchestratorDefinitions } from '@doesrobbiedream/orchestrator-commons'
import { Stringifier } from '@doesrobbiedream/ts-utils'
import Procedure = OrchestratorDefinitions.Procedure
import Composition = OrchestratorDefinitions.Composition
import Transaction = OrchestratorDefinitions.Transaction

@Controller('orchestrator-server')
export class OrchestratorServerController {
  public static prefix = 'orchestrator'
  protected stringifier = new Stringifier()

  constructor(
    protected service: OrchestratorServerService,
    @Inject(CONTEXT) protected context: RequestContext
  ) {}

  @MessagePattern(`${OrchestratorServerController.prefix}.procedure`)
  procedure({ procedure, data }: any) {
    const p: Procedure<any> = this.stringifier.parse(procedure) as Procedure<any>
    this.context.context.args = [p.id]
    return this.service.procedure(p, data)
  }

  @MessagePattern(`${OrchestratorServerController.prefix}.composition`)
  composition({ procedure, data }: any) {
    const c: Composition<any> = this.stringifier.parse(procedure) as Composition<any>
    this.context.context.args = [c.id]
    return this.service.composition(c, data)
  }

  @MessagePattern(`${OrchestratorServerController.prefix}.transaction`)
  transaction({ procedure, data }: any) {
    const t: Transaction<any> = this.stringifier.parse(procedure) as Transaction<any>
    this.context.context.args = [t.id]
    return this.service.transaction(t, data)
  }
}
