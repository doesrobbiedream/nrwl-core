import { Inject, Injectable, Logger } from '@nestjs/common'
import { Exception, FromRPCException, MessageBrokerProxyService } from '@doesrobbiedream/nest-core'
import { OrchestratorDefinitions } from '@doesrobbiedream/orchestrator-commons'
import { BehaviorSubject, from, Observable, of, throwError } from 'rxjs'
import {
  catchError,
  combineAll,
  concatMap,
  map,
  switchMap,
  tap,
  withLatestFrom
} from 'rxjs/operators'
import { OrchestratorServerConfigs } from './orchestrator-server.configs'
import { randHex } from '@doesrobbiedream/ts-utils'
import Procedure = OrchestratorDefinitions.Procedure
import PROXY_PROVIDER = OrchestratorServerConfigs.PROXY_PROVIDER
import OrchestrationStep = OrchestratorDefinitions.OrchestrationStep
import Composition = OrchestratorDefinitions.Composition
import Transaction = OrchestratorDefinitions.Transaction

@Injectable()
export class OrchestratorServerService {
  constructor(@Inject(PROXY_PROVIDER) protected proxy: MessageBrokerProxyService) {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  procedure(procedure: Procedure<any>, data: any) {
    const collected$ = new BehaviorSubject({ input: data })
    const source$ = from(procedure.steps)
    const procedureID = randHex(16)
    return this.chainedSteps$(procedureID, source$, collected$).pipe(
      catchError((e) => {
        return throwError(new FromRPCException(e))
      }),
      withLatestFrom(collected$),
      map((values) => ({ response: values[0], collected: values[1] })),
      map(({ collected }) => procedure.response(collected))
    )
  }

  composition(composition: Composition<any>, data: any) {
    throw new Exception(
      {
        status: 500,
        exception: 'ORCHESTRATOR_EXCEPTION',
        message: 'This is an orchestration exception that should be addressed as it is!'
      },
      { composition, data }
    )
  }

  transaction(transaction: Transaction<any>, data: any) {
    throw new Exception(
      {
        status: 500,
        exception: 'ORCHESTRATOR_EXCEPTION',
        message: 'This is an orchestration exception that should be addressed as it is!'
      },
      { transaction, data }
    )
  }

  protected chainedSteps$(
    procedureID: string,
    source$: Observable<OrchestrationStep>,
    collected$: BehaviorSubject<Record<string & 'input', any>>
  ): Observable<any> {
    return source$.pipe(
      // MAKE THE CALL
      concatMap((source) =>
        of(source).pipe(
          withLatestFrom(collected$),
          map(([source, collected]) => {
            const occurrences = Object.keys(collected$.value).filter((k) =>
              k.startsWith(source.pattern)
            ).length
            const index = occurrences === 0 ? source.pattern : `${source.pattern}_${occurrences}`
            return { ...source, data: source.input(collected), index }
          }),
          switchMap((source) => {
            return this.proxy.client[source.silent ? 'emit' : 'send'](
              source.pattern,
              source.data
            ).pipe(map((response) => ({ ...source, response })))
          }),
          // ADD RESPONSE TO COLLECTED
          tap((source) => {
            collected$.next({ ...collected$.value, [source.index]: source.response })
          }),
          // REGISTER STEP END
          tap((source) => {
            return (
              !source.silent &&
              Logger.log(
                `${procedureID} | ${source.silent ? 'SILENT STEP' : 'PROCESSED STEP'} | ${
                  source.index
                } | ${JSON.stringify(source.data)} | ${JSON.stringify(source.response)}`
              )
            )
          }),
          map(({ response }) => of(response))
        )
      ),
      combineAll()
    )
  }
}
