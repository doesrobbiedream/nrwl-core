import { Test, TestingModule } from '@nestjs/testing'
import { OrchestratorServerController } from './orchestrator-server.controller'
import { OrchestratorServerService } from './orchestrator-server.service'

describe('OrchestratorServerController', () => {
  let controller: OrchestratorServerController

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OrchestratorServerController],
      providers: [{ provide: OrchestratorServerService, useValue: {} }]
    }).compile()

    controller = module.get<OrchestratorServerController>(OrchestratorServerController)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })
})
