import { ConfigService } from '@nestjs/config'
import {
  ClassProvider,
  ExistingProvider,
  ValueProvider
} from '@nestjs/common/interfaces/modules/provider.interface'
import { MessageBrokerProxyService } from '@doesrobbiedream/nest-core'
import { FactoryProvider } from '@nestjs/common'

export namespace OrchestratorServerConfigs {
  export const ORCHESTRATOR_SERVER_CONFIG = 'ORCHESTRATOR_SERVER_CONFIG'
  export const PROXY_PROVIDER = 'PROXY_PROVIDER'

  export interface ModuleConfig {
    proxy_provider:
      | ClassProvider<MessageBrokerProxyService>
      | ValueProvider<MessageBrokerProxyService>
      | FactoryProvider<MessageBrokerProxyService>
      | ExistingProvider<MessageBrokerProxyService>
    imports?: any[]
  }

  export const MONGO_ROOT_CONFIG = (config: ConfigService) => ({
    uri: `${config.get<string>('ORCHESTRATION_DB_HOST') || 'mongodb://mongo:27017'}`,
    dbName: config.get<string>('ORCHESTRATION_DB_NAME') || 'orchestration',
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
  })
}
