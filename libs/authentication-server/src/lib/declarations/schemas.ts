import { Document, Schema, SchemaOptions } from 'mongoose'
import { MongoDocument } from '@doesrobbiedream/nest-core'
import { genSaltSync, hashSync } from 'bcrypt-nodejs'
import { Authentication } from '@doesrobbiedream/authentication-commons'
import User = Authentication.User
import LocalUser = Authentication.LocalUser
import OAuthUser = Authentication.OAuthUser
import OAuthProvider = Authentication.OAuthProvider
import OAUTH_PROVIDER = Authentication.OAUTH_PROVIDER
import MongooseTypes = Schema.Types
import RefreshToken = Authentication.RefreshToken

/* *********************************************************************************************************************
 * INTERFACE DEFINITIONS ***********************************************************************************************
 * ****************************************************************************************************************** */
export interface UserDocument extends User, MongoDocument {}

export interface LocalUserDocument extends UserDocument, LocalUser {}

export interface OAuthUserDocument extends UserDocument, OAuthUser {}

export interface OAuthProviderDocument extends OAuthProvider, MongoDocument {}

export interface RefreshTokenDocument extends RefreshToken, MongoDocument {}

/* *********************************************************************************************************************
 * SCHEMA DEFINITIONS **************************************************************************************************
 * ****************************************************************************************************************** */
const UserSchemaOptions: SchemaOptions = { timestamps: true, discriminatorKey: 'user_type' }

export const UserSchema = new Schema<Document & UserDocument>(
  {
    first_name: { type: String, required: true },
    last_name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    username: { type: String, required: true, unique: true },
    validated: { type: Boolean, required: true }
  },
  UserSchemaOptions
)

export const LocalUserSchema = new Schema<Document & LocalUserDocument>(
  {
    active: { type: Boolean, required: true, default: true },
    validation_token: { type: String, required: true },
    validation_token_expiration_date: { type: Date, required: true },
    password: { type: String, required: true },
    password_reset_token: { type: String },
    password_reset_token_expiration: { type: Date }
  },
  UserSchemaOptions
)

LocalUserSchema.pre('save', function (next) {
  if (this.get('password')) {
    this.set('password', hashSync(this.get('password'), genSaltSync(10)))
  }
  next()
})

export const OAuthProviderSchema = new Schema<Document & OAuthProviderDocument>(
  {
    uuid: { type: String, required: true },
    access_token: { type: String, required: true },
    provider: { type: String, enum: Object.values(OAUTH_PROVIDER), required: true },
    profile_data: { type: MongooseTypes.Mixed, required: true, default: {} }
  },
  UserSchemaOptions
)

export const OAuthUserSchema = new Schema<Document & OAuthUserDocument>(
  {
    active: { type: Boolean, required: true, default: true },
    providers: { type: [OAuthProviderSchema], required: true, default: [] }
  },
  UserSchemaOptions
)

export const RefreshTokenSchema = new Schema<Document & RefreshTokenDocument>(
  {
    user: { type: String, required: true },
    refresh_token: { type: String, required: true },
    jwt: { type: String, required: true },
    expires: { type: Date, required: true, update: false },
    valid: { type: Boolean, default: true }
  },
  { timestamps: true }
)
