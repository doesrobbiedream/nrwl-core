import { Expose, Transform, Type } from 'class-transformer'
import { IsEmail, IsNotEmpty, IsOptional, IsString, Matches } from 'class-validator'
import { Types } from 'mongoose'
import { MongooseUtils } from '@doesrobbiedream/nest-core'

export namespace AuthenticationServerApi {
  export namespace Declarations {
    export interface IGetUser {
      _id?: string
      username?: string
      email?: string
    }
  }
  export namespace DTOs {
    export class UpdatableFields {
      @IsNotEmpty()
      _id!: string
      @IsString()
      @IsOptional()
      first_name?: string
      @IsString()
      @IsOptional()
      last_name?: string
      @IsString()
      @IsOptional()
      username?: string
    }

    export class GetUserDTO {
      @IsOptional()
      @IsString()
      _id?: string
      @IsOptional()
      @IsString()
      username?: string
      @IsOptional()
      @IsString()
      email?: string

      @IsNotEmpty()
      get identifier() {
        return this._id || this.username || this.email
      }
    }

    export class CreateEmailValidationTokenDTO {
      @IsNotEmpty()
      @IsEmail()
      email!: string
    }

    export class ValidateUserDTO {
      @IsNotEmpty()
      @IsString()
      token: string
    }

    export class RemoveUserDTO {
      @IsNotEmpty()
      @IsString()
      _id!: string
    }

    export class LocalUserCreateDTO {
      @IsNotEmpty()
      @IsString()
      first_name!: string

      @IsNotEmpty()
      @IsString()
      last_name!: string

      @IsNotEmpty()
      @IsString()
      email!: string

      @IsNotEmpty()
      @IsString()
      @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[.!@#$%^&*])(?=.{8,})/)
      password!: string
    }

    export class ResetPasswordDTO {
      @IsNotEmpty()
      @IsString()
      token!: string

      @IsNotEmpty()
      @IsString()
      @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[.!@#$%^&*])(?=.{8,})/)
      password!: string
    }

    export class CreateResetPasswordTokenDTO {
      @IsNotEmpty()
      @IsEmail()
      email!: string
    }

    export class GetCredentialDTO {
      @IsOptional()
      @IsString()
      username?: string
      @IsOptional()
      @IsString()
      email?: string

      @IsNotEmpty()
      @IsString()
      password: string

      @IsNotEmpty()
      get identifier() {
        return this.username || this.email
      }
    }

    export class SignCredentialsDTO {
      @IsNotEmpty()
      @IsString()
      _id: string
    }

    export class RefreshTokenDTO {
      @IsNotEmpty()
      @IsString()
      _id: string
      @IsNotEmpty()
      @IsString()
      refresh_token: string
      @IsNotEmpty()
      @IsString()
      jwt: string
    }
  }
  export namespace Responses {
    import IDTransform = MongooseUtils.IDTransform
    export type UserResponseList = Array<UserResponse>

    export class UserResponse {
      @Transform(IDTransform)
      @Expose()
      _id?: Types.ObjectId
      @Expose() createdAt!: Date
      @Expose() email!: string
      @Expose() username!: string
      @Expose() first_name!: string
      @Expose() last_name!: string
      @Expose() updatedAt!: Date
      @Expose() validated!: boolean
    }

    export class CreateUserResponse {
      @Type(() => UserResponse)
      @Expose()
      user!: UserResponse
      @Expose()
      validation_token!: string
    }

    export class NewPasswordResetToken {
      @Expose() password_reset_token!: string
    }

    export class NewEmailValidationToken {
      @Expose() validation_token!: string
    }

    export class UserCredentials {
      @Expose()
      jwt: string
    }
  }
}
