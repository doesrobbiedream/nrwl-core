export namespace AuthenticationServer {
  export enum USER_TYPES {
    LOCAL = 'LOCAL',
    OAUTH = 'OAUTH'
  }
}
