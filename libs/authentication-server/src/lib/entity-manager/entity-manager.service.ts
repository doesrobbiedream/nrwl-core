import { BadRequestException, Inject, Injectable } from '@nestjs/common'
import add from 'date-fns/add'
import { randHex } from '@doesrobbiedream/ts-utils'
import { AuthenticationServerApi } from '../declarations/authentication-server.api'
import { UserNotFoundException } from '@doesrobbiedream/authentication-commons'
import { throwError } from 'rxjs'
import { FilterQuery } from 'mongoose'
import { UserDocument } from '../declarations/schemas'
import { UserRepository } from '../repositories/user.repository'
import { LocalUserRepository } from '../repositories/local-user.repository'
import UserCreateUpdatableFields = AuthenticationServerApi.DTOs.UpdatableFields
import LocalUserCreateDTO = AuthenticationServerApi.DTOs.LocalUserCreateDTO
import IGetUser = AuthenticationServerApi.Declarations.IGetUser

@Injectable()
export class EntityManagerService {
  constructor(
    @Inject(UserRepository) protected users: UserRepository,
    @Inject(LocalUserRepository) protected localUsers: LocalUserRepository
  ) {}

  public getUser(user: IGetUser): Promise<UserDocument> {
    return this.users
      .findByCondition({
        $or: [{ _id: user._id }, { username: user.username }, { email: user.email }]
      })
      .then(
        (u: any[]) => (u.length && u[0]) || throwError(new UserNotFoundException({ input: user }))
      )
  }

  public listUsers(query: FilterQuery<any>) {
    return this.users.findByCondition(query)
  }

  public async createLocalUser(user: LocalUserCreateDTO) {
    return this.localUsers
      .create({
        ...user,
        username: await this.secureUsername(user.email.split('@')[0]),
        validated: false,
        validation_token: randHex(32),
        validation_token_expiration_date: add(new Date(), { days: 7 })
      })
      .then((u) => ({ user: u, validation_token: u.validation_token }))
  }

  public updateUser(user: UserCreateUpdatableFields) {
    return this.users
      .findAndUpdate({ _id: user._id }, user)
      .then((u: any) => u || throwError(new UserNotFoundException({ input: { _id: user._id } })))
  }

  public removeUser(_id: string) {
    return this.users
      .remove(_id)
      .then((u: any) => u || throwError(new UserNotFoundException({ input: { _id } })))
  }

  public validateUserEmail(token: string) {
    return this.localUsers
      .findAndUpdate(
        {
          validated: false,
          validation_token: token,
          validation_token_expiration_date: { $gt: new Date() }
        },
        {
          validated: true,
          validation_token: null,
          validation_token_expiration_date: null
        }
      )
      .then((u) => u || throwError(new BadRequestException()))
  }

  public createEmailValidationToken(email: string) {
    return this.localUsers
      .findAndUpdate(
        { email },
        {
          validated: false,
          validation_token: randHex(64),
          validation_token_expiration_date: add(new Date(), { hours: 6 })
        }
      )
      .then((u) => u || throwError(new UserNotFoundException({ input: { email } })))
  }

  public createResetPasswordToken(email: string) {
    return this.localUsers
      .findAndUpdate(
        { email },
        {
          password_reset_token: randHex(64),
          password_reset_token_expiration: add(new Date(), { hours: 1 })
        }
      )
      .then((u) => u || throwError(new UserNotFoundException({ input: { email } })))
  }

  public resetPassword(token: string, password: string) {
    return this.localUsers
      .findAndUpdate(
        {
          password_reset_token: token,
          password_reset_token_expiration: { $gt: new Date() }
        },
        { password, password_reset_token: null, password_reset_token_expiration: null }
      )
      .then((u) => u || throwError(new BadRequestException()))
  }

  protected secureUsername(username: string) {
    const rx = new RegExp(`^${username}`)
    return this.users
      .findByCondition({ username: rx })
      .then((docs: any[]) => (docs.length > 0 ? username + docs.length : username))
  }
}
