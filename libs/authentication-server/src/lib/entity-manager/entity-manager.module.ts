import { Module } from '@nestjs/common'
import { EntityManagerService } from './entity-manager.service'
import { EntityManagerController } from './entity-manager.controller'
import { ConfigModule } from '@nestjs/config'
import { RepositoriesModule } from '../repositories/repositories.module'

@Module({
  imports: [ConfigModule, RepositoriesModule],
  controllers: [EntityManagerController],
  providers: [EntityManagerService]
})
export class EntityManagerModule {}
