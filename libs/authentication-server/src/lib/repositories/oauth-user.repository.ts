import { MongoRepository } from '@doesrobbiedream/nest-core'
import { Inject, Injectable } from '@nestjs/common'
import { AuthenticationServer } from '../declarations/authentication-server.declarations'
import { Model } from 'mongoose'
import { Authentication } from '@doesrobbiedream/authentication-commons'
import OAuthUser = Authentication.OAuthUser
import USER_TYPES = AuthenticationServer.USER_TYPES

@Injectable()
export class OAuthUserRepository extends MongoRepository<OAuthUser> {
  constructor(@Inject(USER_TYPES.OAUTH) entity: Model<OAuthUser>) {
    super(entity)
  }
}
