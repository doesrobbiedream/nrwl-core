import { Module } from '@nestjs/common'
import { CredentialsManagerController } from './credentials-manager.controller'
import { CredentialsManagerService } from './credentials-manager.service'
import { RepositoriesModule } from '../repositories/repositories.module'
import { ConfigModule } from '@nestjs/config'

@Module({
  imports: [ConfigModule, RepositoriesModule],
  controllers: [CredentialsManagerController],
  providers: [CredentialsManagerService]
})
export class CredentialsManagerModule {}
