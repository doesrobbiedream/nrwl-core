import { MongooseModule } from '@nestjs/mongoose'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { Module } from '@nestjs/common'
import { CredentialsManagerModule } from './credentials-manager/credentials-manager.module'
import { EntityManagerModule } from './entity-manager/entity-manager.module'
import { MONGO_ROOT_CONFIG } from './declarations/db_connection.declarations'
import { EXCEPTION_FILTER_PROVIDER } from '@doesrobbiedream/nest-core'
import { AuthenticationExceptionsDictionary } from '@doesrobbiedream/authentication-commons'

@Module({
  imports: [
    ConfigModule,
    MongooseModule.forRootAsync({
      connectionName: 'authentication-server',
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => MONGO_ROOT_CONFIG(config),
      inject: [ConfigService]
    }),
    CredentialsManagerModule,
    EntityManagerModule
  ],
  providers: [EXCEPTION_FILTER_PROVIDER(AuthenticationExceptionsDictionary)]
})
export class AuthenticationServerModule {}
