## @doesrobbiedream/authentication-server [1.4.5](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-server@1.4.4...@doesrobbiedream/authentication-server@1.4.5) (2021-07-29)





### Dependencies

* **@doesrobbiedream/authentication-commons:** upgraded to 1.3.2
* **@doesrobbiedream/nest-core:** upgraded to 1.1.2

## @doesrobbiedream/authentication-server [1.4.4](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-server@1.4.3...@doesrobbiedream/authentication-server@1.4.4) (2021-07-29)





### Dependencies

* **@doesrobbiedream/authentication-commons:** upgraded to 1.3.1
* **@doesrobbiedream/nest-core:** upgraded to 1.1.1

## @doesrobbiedream/authentication-server [1.4.3](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-server@1.4.2...@doesrobbiedream/authentication-server@1.4.3) (2021-07-16)


### Bug Fixes

* **authentication-client:** fix dynamic expiration time on jwt and refresh token ([073aa07](https://gitlab.com/doesrobbiedream/nrwl-core/commit/073aa0762f3d72deae708be4358e1d2fd9a2dea4))

## @doesrobbiedream/authentication-server [1.4.2](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-server@1.4.1...@doesrobbiedream/authentication-server@1.4.2) (2021-07-16)


### Bug Fixes

* **authentication-client:** fix dynamic expiration time on jwt and refresh token ([115b176](https://gitlab.com/doesrobbiedream/nrwl-core/commit/115b176440042391680a4c75184a1883ba3a6ad8))

## @doesrobbiedream/authentication-server [1.4.1](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-server@1.4.0...@doesrobbiedream/authentication-server@1.4.1) (2021-07-09)


### Bug Fixes

* **authentication-server:** fix user.validated type string -> bool ([02d25f7](https://gitlab.com/doesrobbiedream/nrwl-core/commit/02d25f79d85e24d0fc9bcfe805ef6630f1e21af1))

# @doesrobbiedream/authentication-server [1.4.0](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-server@1.3.0...@doesrobbiedream/authentication-server@1.4.0) (2021-07-01)


### Features

* **authentication-server:** finish authentication server calls implementation ([8880e64](https://gitlab.com/doesrobbiedream/nrwl-core/commit/8880e6484b968a7739b0f702744c279849a315db))

# @doesrobbiedream/authentication-server [1.3.0](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-server@1.2.0...@doesrobbiedream/authentication-server@1.3.0) (2021-06-29)


### Bug Fixes

* **authentication-server:** make refresh-token throw EXPIRED_CREDENTIALS exception ([b3834fa](https://gitlab.com/doesrobbiedream/nrwl-core/commit/b3834fad9fd56c900f90de528f2e737dd1af37b6))


### Features

* **authentication-commons:** add expired-credential exception ([ef997d3](https://gitlab.com/doesrobbiedream/nrwl-core/commit/ef997d35cf5bf4234676366835064a0b343e0380))





### Dependencies

* **@doesrobbiedream/authentication-commons:** upgraded to 1.3.0

# @doesrobbiedream/authentication-server [1.2.0](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-server@1.1.0...@doesrobbiedream/authentication-server@1.2.0) (2021-06-29)


### Features

* **authentication-server:** add jwt to RefreshTokenSchema ([88a26dc](https://gitlab.com/doesrobbiedream/nrwl-core/commit/88a26dc8b058257f9bc85e72d61fcbcfc0b3e6a5))
* **authentication-server:** implement refresh-token flow ([239f71d](https://gitlab.com/doesrobbiedream/nrwl-core/commit/239f71df225004705cee7cfaad8ff7be8edee45e))





### Dependencies

* **@doesrobbiedream/authentication-commons:** upgraded to 1.2.0

# @doesrobbiedream/authentication-server [1.1.0](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-server@1.0.0...@doesrobbiedream/authentication-server@1.1.0) (2021-06-28)


### Features

* **authentication-client,authentication-commons,authentication-server,nest-core:** m ([f6a6099](https://gitlab.com/doesrobbiedream/nrwl-core/commit/f6a609934086444d1d8de836ce6db01aa7215eac))





### Dependencies

* **@doesrobbiedream/authentication-commons:** upgraded to 1.1.0
* **@doesrobbiedream/nest-core:** upgraded to 1.1.0

# @doesrobbiedream/authentication-server 1.0.0 (2021-06-17)


### Bug Fixes

* **authentication-server:** remove absolute import to pass module-boundaries lint ([24d6d41](https://gitlab.com/doesrobbiedream/nrwl-core/commit/24d6d41b5098d18a358d7759f907c583a259bfce))


### Features

* **authentication-server:** add test coverage to entity-manager service ([c8a6e26](https://gitlab.com/doesrobbiedream/nrwl-core/commit/c8a6e263f0d6ad4e10e79c00d9cabb1a7b1d7882))
* **authentication-server:** implement controller tests and define module API ([2bb190b](https://gitlab.com/doesrobbiedream/nrwl-core/commit/2bb190b7250a20135b35274c2798d5d551deaa92))
* **authentication-server:** implement controller tests and define module API ([580f175](https://gitlab.com/doesrobbiedream/nrwl-core/commit/580f1751db7b68136a3ae5c274b40d61f05da431))
* **master:** automated library publishing ([b85d932](https://gitlab.com/doesrobbiedream/nrwl-core/commit/b85d93299a57fad10f0de27c35b27e547f93c657))





### Dependencies

* **@doesrobbiedream/authentication-commons:** upgraded to 1.0.0
* **@doesrobbiedream/nest-core:** upgraded to 1.0.0
* **@doesrobbiedream/ts-utils:** upgraded to 1.0.0
