import { Inject, Injectable } from '@nestjs/common'
import {
  ORCHESTRATOR_CONFIG,
  OrchestratorOptions,
  PROXY_PROVIDER
} from './orchestrator-client.config'
import { OrchestratorDefinitions } from '@doesrobbiedream/orchestrator-commons'
import { throwError } from 'rxjs'
import { catchError } from 'rxjs/operators'
import { Stringifier } from '@doesrobbiedream/ts-utils'
import { FromRPCException, MessageBrokerProxyService } from '@doesrobbiedream/nest-core'
import Procedure = OrchestratorDefinitions.Procedure
import Composition = OrchestratorDefinitions.Composition
import Transaction = OrchestratorDefinitions.Transaction
import OrchestratorClient = OrchestratorDefinitions.OrchestratorClient

@Injectable()
export class OrchestratorClientService implements OrchestratorClient {
  constructor(
    @Inject(ORCHESTRATOR_CONFIG) protected config: OrchestratorOptions,
    @Inject(PROXY_PROVIDER) protected proxy: MessageBrokerProxyService
  ) {}

  public procedure<T>(procedure: Procedure<T>, data: any) {
    return this.proxy.client
      .send<T>(`orchestrator.procedure`, {
        procedure: new Stringifier().stringify(procedure),
        data
      })
      .pipe(catchError((exception) => throwError(new FromRPCException(exception))))
  }

  public composition<T>(procedure: Composition<T>, data: any) {
    return this.proxy.client
      .send<T>(`orchestrator.composition`, {
        procedure: new Stringifier().stringify(procedure),
        data
      })
      .pipe(catchError((exception) => throwError(new FromRPCException(exception))))
  }

  public transaction<T>(procedure: Transaction<T>, data: any) {
    return this.proxy.client
      .send<T>(`orchestrator.transaction`, {
        procedure: new Stringifier().stringify(procedure),
        data
      })
      .pipe(catchError((exception) => throwError(new FromRPCException(exception))))
  }
}
