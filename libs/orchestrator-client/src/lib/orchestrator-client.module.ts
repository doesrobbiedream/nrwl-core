import { DynamicModule, Module } from '@nestjs/common'
import {
  ORCHESTRATOR_CONFIG,
  OrchestratorClientConfig,
  PROXY_PROVIDER
} from './orchestrator-client.config'
import { OrchestratorClientService } from './orchestrator-client.service'

@Module({
  providers: [OrchestratorClientService]
})
export class OrchestratorClientModule {
  public static register(config: OrchestratorClientConfig): DynamicModule {
    return {
      global: true,
      module: OrchestratorClientModule,
      imports: config.imports,
      providers: [
        {
          provide: ORCHESTRATOR_CONFIG,
          useValue: config.options
        },
        { ...config.proxy_provider, provide: PROXY_PROVIDER },
        OrchestratorClientService
      ],
      exports: [OrchestratorClientService]
    }
  }
}
