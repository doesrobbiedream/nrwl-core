import { Test, TestingModule } from '@nestjs/testing'
import { OrchestratorClientService } from './orchestrator-client.service'
import { ORCHESTRATOR_CONFIG, PROXY_PROVIDER } from './orchestrator-client.config'

describe('OrchestratorClientService', () => {
  let service: OrchestratorClientService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: PROXY_PROVIDER,
          useValue: {}
        },
        {
          provide: ORCHESTRATOR_CONFIG,
          useValue: {}
        },
        OrchestratorClientService
      ]
    }).compile()

    service = module.get<OrchestratorClientService>(OrchestratorClientService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })
})
