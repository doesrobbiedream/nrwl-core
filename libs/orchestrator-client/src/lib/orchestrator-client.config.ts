import {
  ClassProvider,
  ExistingProvider,
  FactoryProvider,
  Type,
  ValueProvider
} from '@nestjs/common'
import { MessageBrokerProxyService } from '@doesrobbiedream/nest-core'

export interface OrchestratorClientConfig {
  proxy_provider:
    | ClassProvider<MessageBrokerProxyService>
    | ValueProvider<MessageBrokerProxyService>
    | FactoryProvider<MessageBrokerProxyService>
    | ExistingProvider<MessageBrokerProxyService>
  options: OrchestratorOptions
  imports?: any[]
}

export interface OrchestratorOptions {
  service: OrchestratorServiceConfig
}

export interface OrchestratorServiceConfig {
  exception: Type<any>
}

export const PROXY_PROVIDER = 'PROXY_PROVIDER'
export const ORCHESTRATOR_CONFIG = 'ORCHESTRATOR_CONFIG'
