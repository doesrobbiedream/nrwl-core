/**
 * @module Constants
 */

/**
 * The token to store the root instance of commander
 */
export const CLI_SERVICE_TOKEN = 'nests-console.Cli'

export const CONSOLE_SERVICE_TOKEN = 'nests-console.Service'

/**
 * The key to store metadata of a Console decorator
 */
export const CONSOLE_METADATA_NAME = 'nests-console.Console'

/**
 * The key to store metadata of a Command decorator
 */
export const COMMAND_METADATA_NAME = 'nests-console.Command:'
