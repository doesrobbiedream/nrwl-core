import { Observable } from 'rxjs'

export namespace OrchestratorDefinitions {
  export interface OrchestratorClient {
    procedure<T>(procedure: Procedure<T>, data: any): Observable<T>

    composition<T>(procedure: Composition<T>, data: any): Observable<T>

    transaction<T>(procedure: Transaction<T>, data: any): Observable<T>
  }

  export type CollectedData = Record<string & 'input', any>

  export interface OrchestrationStep<Input = any> {
    pattern: string
    silent?: boolean
    input: (collected: CollectedData) => Input
  }

  export interface CompositionStep<T = any> extends OrchestrationStep<T> {
    silent?: true
  }

  export interface ProcedureStep<T = any> extends OrchestrationStep<T> {
    emit?: false | string
  }

  export interface Orchestration<Composition = any> {
    id: string
    steps: OrchestrationStep[]
    response: (collected: CollectedData) => Composition
  }

  export interface Composition<T> extends Orchestration {
    steps: CompositionStep<T>[]
  }

  export interface Procedure<T> extends Orchestration {
    steps: ProcedureStep<T>[]
  }

  export interface Transaction<T = any> extends Orchestration<T> {
    rollback: any
  }
}
