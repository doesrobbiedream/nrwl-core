import { ColorUtils } from './color.utils'

describe('Color Utils', () => {
  it('should return 1 for white luminance', () => {
    expect(ColorUtils.luminance('white')).toEqual(1)
  })

  it('should return 0 for black luminance', () => {
    expect(ColorUtils.luminance('black')).toEqual(0)
  })

  it('should return 0.01441 for #2D1D02', () => {
    expect(ColorUtils.luminance('#2D1D02').toPrecision(4)).toEqual(String(0.01441))
  })
})
