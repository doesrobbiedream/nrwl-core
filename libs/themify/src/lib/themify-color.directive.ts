import { Directive, ElementRef, Input, OnChanges, Renderer2 } from '@angular/core'
import { ThemifyService } from './themify.service'
import _forEach from 'lodash/forEach'
import _pickBy from 'lodash/pickBy'
import _identity from 'lodash/identity'

@Directive({
  selector: '[drdThemifyColor], [drd-themify-color]'
})
export class ThemifyColorDirective implements OnChanges {
  @Input('set-background') background!: string
  @Input('set-color') color!: string
  @Input('set-border') border!: string

  constructor(
    protected themify: ThemifyService,
    protected el: ElementRef,
    protected renderer: Renderer2
  ) {}

  ngOnChanges() {
    this.applyStyles()
  }

  applyStyles() {
    const dictionary = {
      'border-color': this.border && this.border.split(','),
      'background-color': this.background && this.background.split(','),
      color: this.color && this.color.split(',')
    }
    _forEach(_pickBy(dictionary, _identity), ([type, accessor], index) =>
      this.renderer.setStyle(this.el.nativeElement, index, this.themify.colorVar(type, accessor))
    )
  }
}
