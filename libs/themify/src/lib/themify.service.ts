import { Inject, Injectable } from '@angular/core'
import { ThemifyConfigurationInterface } from './themify-configuration.interface'
import { CONFIG_TOKEN } from './config.token'
import _reduce from 'lodash/reduce'
import _forEach from 'lodash/forEach'
import { ColorSchema } from './color-schema'
import Color from 'color'

@Injectable({
  providedIn: 'root'
})
export class ThemifyService {
  protected colors: { [key: string]: ColorSchema }
  protected color_values: { [key: string]: Color }

  constructor(@Inject(CONFIG_TOKEN) protected configuration: ThemifyConfigurationInterface) {
    this.colors = _reduce(
      configuration.colors,
      (colors, color, identifier) => ({
        ...colors,
        ...{ [identifier]: new ColorSchema(identifier, color) }
      }),
      {}
    )
    this.color_values = _reduce(
      configuration.color_values,
      (colors, color, identifier) => ({
        ...colors,
        ...{ [identifier]: new Color(color) }
      }),
      {}
    )
  }

  initializeColors() {
    _forEach(this.colors, (color: ColorSchema) => {
      _forEach(color.cssVariables(), (color, property) => {
        document.documentElement.style.setProperty(property, color)
      })
    })
    _forEach(this.color_values, (color: Color, identifier: string) => {
      document.documentElement.style.setProperty(this.color_value_var(identifier), color.hex())
    })
  }

  color(identifier: string, accessor: string = 'base') {
    return this.colors[identifier].color(accessor)
  }

  colorVar(identifier: string, accessor: string = 'base') {
    return this.colors[identifier].cssVar(accessor)
  }

  color_value(identifier: string) {
    return this.color_values[identifier]
  }

  color_value_var(identifier: string) {
    return `--${identifier}`
  }
}
