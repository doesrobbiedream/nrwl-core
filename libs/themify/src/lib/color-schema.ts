import Color from 'color'
import _reduce from 'lodash/reduce'
import { ColorUtils } from './color.utils'
import luminance = ColorUtils.luminance

export class ColorSchema {
  public static readonly ACCESSORS = ['base', 'light', 'dark']
  public static readonly ACCESSORS_BUILDER = [
    (c: Color) => c,
    (c: Color) => c.lighten(1 - Math.pow(luminance(c.hex()), 1 / 2)),
    (c: Color) => c.darken(Math.pow(luminance(c.hex()), 1 / 2))
  ]
  protected accessors: { [key: string]: Color }
  protected contrasts: { [key: string]: Color }
  protected gradient: { [key: string]: Color }
  protected palette: { [key: string]: Color }

  constructor(protected identifier: string, base: string) {
    const _base = Color(base)
    this.gradient = _reduce(
      ColorUtils.gradient(_base.hex()),
      (gradients, color, name) => ({ ...gradients, ...{ [name]: Color(color) } }),
      {}
    )
    this.accessors = _reduce(
      ColorSchema.ACCESSORS_BUILDER,
      (accessors, builder, index) => ({
        ...accessors,
        ...{ [ColorSchema.ACCESSORS[index]]: builder(_base) }
      }),
      {}
    )
    this.contrasts = _reduce(
      this.accessors,
      (contrasts, color: Color, name) => ({
        ...contrasts,
        ...{ [`${name}-contrast`]: Color(ColorUtils.contrast(color.hex())) }
      }),
      {}
    )
    this.palette = { ...this.accessors, ...this.gradient, ...this.contrasts }
  }

  cssVariables(): Record<string, string> {
    return _reduce(
      this.palette,
      (variables, color: Color, accessor) => ({
        ...variables,
        ...{ [`--${this.identifier}-${accessor}`]: color.hex() }
      }),
      {}
    )
  }

  color(accessor: string): Color {
    return this.palette[accessor]
  }

  cssVar(accessor: string): string {
    return `--${this.identifier}-${accessor}`
  }
}
