# @doesrobbiedream/themify [1.1.0](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/themify@1.0.0...@doesrobbiedream/themify@1.1.0) (2021-06-29)


### Features

* **themify:** add color_value for external styles customizations ([689ca45](https://gitlab.com/doesrobbiedream/nrwl-core/commit/689ca45d4c0ccc81694fae3193ef000c31cdd3ef))

# @doesrobbiedream/themify 1.0.0 (2021-06-17)


### Features

* **master:** automated library publishing ([b85d932](https://gitlab.com/doesrobbiedream/nrwl-core/commit/b85d93299a57fad10f0de27c35b27e547f93c657))
