import { AuthGuard } from '@nestjs/passport'
import { ExpiredCredentialsException } from '@doesrobbiedream/authentication-commons'

export class JwtAuthGuard extends AuthGuard('jwt') {
  handleRequest(err, user) {
    if (err || !user) {
      throw new ExpiredCredentialsException()
    }
    return user
  }
}
