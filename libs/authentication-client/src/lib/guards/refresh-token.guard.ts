import { AuthGuard } from '@nestjs/passport'
import { InvalidCredentialsException } from '@doesrobbiedream/authentication-commons'

export class RefreshTokenGuard extends AuthGuard('refresh-token') {
  handleRequest(err, user) {
    if (err || !user) {
      throw new InvalidCredentialsException()
    }
    return user
  }
}
