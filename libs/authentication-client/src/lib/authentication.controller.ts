import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  Post,
  Put,
  Query,
  Request,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags
} from '@nestjs/swagger'
import { MicroserviceMessagingDeclarations } from '@doesrobbiedream/nest-core'
import { AuthenticationClientProcedures } from './authentication-client.procedures'
import { AuthenticationClientConfig } from './authentication-client-module.config'
import { AuthenticationClientAPI } from './authentication-client.api'
import { LocalAuthGuard } from './guards/local.guard'
import { RefreshTokenGuard } from './guards/refresh-token.guard'
import AUTHENTICATION_CLIENT_PROCEDURES = AuthenticationClientProcedures.AUTHENTICATION_CLIENT_PROCEDURES
import PROCEDURES_TOKEN = AuthenticationClientProcedures.PROCEDURES_TOKEN
import BrokersList = MicroserviceMessagingDeclarations.BrokersList
import AUTHENTICATION_CLIENT_CONFIG = AuthenticationClientConfig.AUTHENTICATION_CLIENT_CONFIG
import AuthenticationClientModuleConfig = AuthenticationClientConfig.AuthenticationClientModuleConfig
import BROKERS = AuthenticationClientConfig.BROKERS
import submitToHandler = MicroserviceMessagingDeclarations.submitToHandler
import SignUpDTO = AuthenticationClientAPI.DTO.SignUpDTO
import SignInDTO = AuthenticationClientAPI.DTO.SignInDTO
import CredentialsResponse = AuthenticationClientAPI.Response.CredentialsResponse
import ResendVerificationLinkDTO = AuthenticationClientAPI.DTO.ResendVerificationLinkDTO
import SetNewPasswordDTO = AuthenticationClientAPI.DTO.SetNewPasswordDTO
import ResetPasswordRequestDTO = AuthenticationClientAPI.DTO.ResetPasswordRequestDTO

@ApiTags('Authentication Module')
@Controller('auth')
export class AuthenticationController {
  constructor(
    @Inject(PROCEDURES_TOKEN)
    protected procedures: Record<AUTHENTICATION_CLIENT_PROCEDURES, any>,
    @Inject(BROKERS)
    protected brokers: BrokersList,
    @Inject(AUTHENTICATION_CLIENT_CONFIG)
    protected config: AuthenticationClientModuleConfig
  ) {}

  @Get('user')
  @ApiOperation({ summary: 'List All Available Users' })
  ListUser() {
    return submitToHandler(this.brokers, this.procedures.LIST_USERS, {})
  }

  @Get('user/:id')
  @ApiOperation({ summary: 'Find User By Identifier' })
  FindUser(@Param('id') _id: string) {
    return submitToHandler(this.brokers, this.procedures.FIND_USER, { _id })
  }

  @Post('user')
  @ApiOperation({ summary: 'Register New User On Database' })
  @ApiBody({ type: SignUpDTO })
  SignUp(@Body() user: SignUpDTO) {
    return submitToHandler(this.brokers, this.procedures.SIGN_UP, user)
  }

  @Get('credentials')
  @ApiOperation({ summary: 'Sign In and Retrieve Credentials' })
  @ApiQuery({ type: SignInDTO })
  @ApiResponse({ status: 200, type: CredentialsResponse })
  @UseGuards(LocalAuthGuard)
  SignIn(@Request() { user }: any) {
    return user
  }

  @Put('credentials')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Refresh User Credentials' })
  @ApiResponse({ status: 200, type: CredentialsResponse })
  @UseGuards(RefreshTokenGuard)
  RefreshToken(@Request() { user }) {
    return user
  }

  @Put('email/token')
  @ApiOperation({ summary: 'Validate Account Owner Email by Token' })
  @ApiParam({ name: 'token', type: String })
  ValidateEmail(@Query('token') token: string) {
    return submitToHandler(this.brokers, this.procedures.VALIDATE_EMAIL, { token })
  }

  @Post('email/token')
  @ApiOperation({ summary: 'Resend Account Owner Email Verification Token' })
  @ApiBody({ type: ResendVerificationLinkDTO })
  ResendEmailVerificationLink(@Body() { email }: ResendVerificationLinkDTO) {
    return submitToHandler(this.brokers, this.procedures.RESEND_EMAIL_VERIFICATION, { email })
  }

  @Put('password')
  @ApiOperation({ summary: 'Set New Password' })
  @ApiParam({
    name: 'token',
    type: String,
    description: 'Reset Token sent to Account Owners Email'
  })
  @ApiBody({ type: SetNewPasswordDTO })
  SetNewPassword(@Query('token') token: string, @Body() { password }: SetNewPasswordDTO) {
    return submitToHandler(this.brokers, this.procedures.SET_NEW_PASSWORD, { token, password })
  }

  @Post('password/token')
  @ApiOperation({ summary: 'Request Password Reset' })
  @ApiBody({ type: ResetPasswordRequestDTO })
  RequestPasswordReset(@Body() { email }: ResetPasswordRequestDTO) {
    return submitToHandler(this.brokers, this.procedures.REQUEST_PASSWORD_RESET, { email })
  }
}
