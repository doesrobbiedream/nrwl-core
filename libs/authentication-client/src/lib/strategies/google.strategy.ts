import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { PassportStrategy } from '@nestjs/passport'
import { Strategy } from 'passport-google-oauth20'

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  constructor(config: ConfigService) {
    super({
      clientID: config.get<string>('GOOGLE_AUTH_CLIENT'),
      clientSecret: config.get<string>('GOOGLE_AUTH_SECRET'),
      callbackURL: config.get<string>('GOOGLE_AUTH_CALLBACK'),
      passReqToCallback: true,
      scope: ['email', 'profile', 'openid']
    })
  }

  // async validate(
  //   request: any,
  //   accessToken: string,
  //   refreshToken: string,
  //   profile,
  //   done: (...args: any[]) => void
  // ) {}
}
