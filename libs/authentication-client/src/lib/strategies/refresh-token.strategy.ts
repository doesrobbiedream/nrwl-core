import { Inject, Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { PassportStrategy } from '@nestjs/passport'
import { ExtractJwt, Strategy } from 'passport-jwt'
import { AuthenticationClientProcedures } from '../authentication-client.procedures'
import { AuthenticationClientConfig } from '../authentication-client-module.config'
import { MicroserviceMessagingDeclarations } from '@doesrobbiedream/nest-core'
import PROCEDURES_TOKEN = AuthenticationClientProcedures.PROCEDURES_TOKEN
import AUTHENTICATION_CLIENT_PROCEDURES = AuthenticationClientProcedures.AUTHENTICATION_CLIENT_PROCEDURES
import BROKERS = AuthenticationClientConfig.BROKERS
import BrokersList = MicroserviceMessagingDeclarations.BrokersList
import submitToHandler = MicroserviceMessagingDeclarations.submitToHandler

@Injectable()
export class RefreshTokenStrategy extends PassportStrategy(Strategy, 'refresh-token') {
  constructor(
    @Inject(PROCEDURES_TOKEN)
    protected procedures: Record<AUTHENTICATION_CLIENT_PROCEDURES, any>,
    @Inject(BROKERS)
    protected brokers: BrokersList,
    protected config: ConfigService
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: config.get('JWT_SECRET'),
      ignoreExpiration: true,
      passReqToCallback: true
    })
  }

  async validate(request, { _id, refresh_token }) {
    const old_jwt = request.headers.authorization.split(' ')[1]
    return await submitToHandler(this.brokers, this.procedures.REFRESH_TOKEN, {
      _id,
      refresh_token,
      jwt: old_jwt
    }).toPromise()
  }
}
