import { Strategy } from 'passport-local'
import { PassportStrategy } from '@nestjs/passport'
import { Inject, Injectable } from '@nestjs/common'
import { AuthenticationClientConfig } from '../authentication-client-module.config'
import { AuthenticationClientProcedures } from '../authentication-client.procedures'
import { MicroserviceMessagingDeclarations } from '@doesrobbiedream/nest-core'
import submitToHandler = MicroserviceMessagingDeclarations.submitToHandler

import AUTHENTICATION_CLIENT_CONFIG = AuthenticationClientConfig.AUTHENTICATION_CLIENT_CONFIG
import AuthenticationClientModuleConfig = AuthenticationClientConfig.AuthenticationClientModuleConfig
import PROCEDURES_TOKEN = AuthenticationClientProcedures.PROCEDURES_TOKEN
import AUTHENTICATION_CLIENT_PROCEDURES = AuthenticationClientProcedures.AUTHENTICATION_CLIENT_PROCEDURES
import BROKERS = AuthenticationClientConfig.BROKERS
import BrokersList = MicroserviceMessagingDeclarations.BrokersList

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
  constructor(
    @Inject(PROCEDURES_TOKEN)
    protected procedures: Record<AUTHENTICATION_CLIENT_PROCEDURES, any>,
    @Inject(BROKERS)
    protected brokers: BrokersList,
    @Inject(AUTHENTICATION_CLIENT_CONFIG)
    protected config: AuthenticationClientModuleConfig
  ) {
    super({})
  }

  async validate(username, password) {
    return await submitToHandler(this.brokers, this.procedures.SIGN_IN, {
      username,
      password
    }).toPromise()
  }
}
