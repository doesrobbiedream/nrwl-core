import { Controller, Get } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'
import { NotImplementedException } from '@doesrobbiedream/nest-core'

@ApiTags('Authentication Module')
@Controller('oauth')
export class OauthController {
  @Get('google')
  OAuthGoogleInit() {
    throw new NotImplementedException()
  }

  @Get('google/callback')
  OAuthGoogleCallback() {
    throw new NotImplementedException()
  }
}
