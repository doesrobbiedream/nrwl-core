## @doesrobbiedream/authentication-client [1.4.5](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-client@1.4.4...@doesrobbiedream/authentication-client@1.4.5) (2021-07-29)





### Dependencies

* **@doesrobbiedream/nest-core:** upgraded to 1.1.2
* **@doesrobbiedream/authentication-commons:** upgraded to 1.3.2

## @doesrobbiedream/authentication-client [1.4.4](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-client@1.4.3...@doesrobbiedream/authentication-client@1.4.4) (2021-07-29)





### Dependencies

* **@doesrobbiedream/nest-core:** upgraded to 1.1.1
* **@doesrobbiedream/authentication-commons:** upgraded to 1.3.1

## @doesrobbiedream/authentication-client [1.4.3](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-client@1.4.2...@doesrobbiedream/authentication-client@1.4.3) (2021-07-15)


### Bug Fixes

* **authentication-client:** fix exception handler on RefreshToken AuthGuard ([d1d9156](https://gitlab.com/doesrobbiedream/nrwl-core/commit/d1d9156e71afb32564a4a2dc492942a96f8f6ed8))

## @doesrobbiedream/authentication-client [1.4.2](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-client@1.4.1...@doesrobbiedream/authentication-client@1.4.2) (2021-07-15)


### Bug Fixes

* **authentication-client:** fix exception handler on JWTAuthGuard ([e2b1867](https://gitlab.com/doesrobbiedream/nrwl-core/commit/e2b186743e4d1455c86545c4323dc7b63affffea))

## @doesrobbiedream/authentication-client [1.4.1](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-client@1.4.0...@doesrobbiedream/authentication-client@1.4.1) (2021-07-14)


### Bug Fixes

* **authentication-client:** fix validate method return type Observable -> awaited Promise ([829d218](https://gitlab.com/doesrobbiedream/nrwl-core/commit/829d218a78cc046748f9c43587dea81e4c13edf0))

# @doesrobbiedream/authentication-client [1.4.0](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-client@1.3.0...@doesrobbiedream/authentication-client@1.4.0) (2021-07-01)


### Features

* **authentication-client:** finish authentication controllers implementation ([0c595a1](https://gitlab.com/doesrobbiedream/nrwl-core/commit/0c595a1c86c21124538c836827eb4bda66e5c866))

# @doesrobbiedream/authentication-client [1.3.0](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-client@1.2.0...@doesrobbiedream/authentication-client@1.3.0) (2021-06-29)


### Features

* **authentication-client:** finish controllers setup and default procedures ([0fe33e7](https://gitlab.com/doesrobbiedream/nrwl-core/commit/0fe33e7f498265a9bdabedd55abef43777778274))
* **authentication-commons:** add expired-credential exception ([ef997d3](https://gitlab.com/doesrobbiedream/nrwl-core/commit/ef997d35cf5bf4234676366835064a0b343e0380))





### Dependencies

* **@doesrobbiedream/authentication-commons:** upgraded to 1.3.0

# @doesrobbiedream/authentication-client [1.2.0](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-client@1.1.0...@doesrobbiedream/authentication-client@1.2.0) (2021-06-29)


### Bug Fixes

* **authentication-client:** implement refresh-token guard ([419a73d](https://gitlab.com/doesrobbiedream/nrwl-core/commit/419a73d53a3ff88342d8638ef835fa9bd1951631))


### Features

* **authentication-client:** implement refresh-token guard ([6389eb5](https://gitlab.com/doesrobbiedream/nrwl-core/commit/6389eb5fab39eec5c2827f896f3cbf8a4adc073f))





### Dependencies

* **@doesrobbiedream/authentication-commons:** upgraded to 1.2.0

# @doesrobbiedream/authentication-client [1.1.0](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-client@1.0.0...@doesrobbiedream/authentication-client@1.1.0) (2021-06-28)


### Features

* **authentication-client,authentication-commons,authentication-server,nest-core:** m ([f6a6099](https://gitlab.com/doesrobbiedream/nrwl-core/commit/f6a609934086444d1d8de836ce6db01aa7215eac))





### Dependencies

* **@doesrobbiedream/nest-core:** upgraded to 1.1.0
* **@doesrobbiedream/authentication-commons:** upgraded to 1.1.0

# @doesrobbiedream/authentication-client 1.0.0 (2021-06-17)


### Features

* **master:** automated library publishing ([b85d932](https://gitlab.com/doesrobbiedream/nrwl-core/commit/b85d93299a57fad10f0de27c35b27e547f93c657))





### Dependencies

* **@doesrobbiedream/nest-core:** upgraded to 1.0.0
* **@doesrobbiedream/ts-utils:** upgraded to 1.0.0
* **@doesrobbiedream/authentication-commons:** upgraded to 1.0.0

# Changelog

This file was generated using [@jscutlery/semver](https://github.com/jscutlery/semver).

# 0.1.0 (2021-06-15)


### Features

* **master:** automated library publishing ([b85d932](https://gitlab.com/doesrobbiedream/nrwl-core/commit/b85d93299a57fad10f0de27c35b27e547f93c657))
