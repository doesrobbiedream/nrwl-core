import { RegisteredException } from '@doesrobbiedream/nest-core'

export class UserNotFoundException extends RegisteredException {
  public exception = 'USER_NOT_FOUND'

  constructor(public data?: Record<string, any>) {
    super()
  }
}
