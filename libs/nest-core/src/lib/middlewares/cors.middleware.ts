import { Injectable, Inject, NestMiddleware } from '@nestjs/common'

export const CORS_CONFIG = 'CORS_CONFIG'
export type CorsConfig = string[]

@Injectable()
export class CorsMiddleware implements NestMiddleware {
  constructor(@Inject(CORS_CONFIG) protected allowedOrigins: CorsConfig = []) {}

  use(req: any, res: any, next: () => void) {
    if (this.allowedOrigins.indexOf(req.header('Origin')) > -1) {
      res.header('Access-Control-Allow-Origin', req.header('Origin'))
      res.header('Access-Control-Allow-Headers', 'content-type')
      res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS')
    }
    next()
  }
}
