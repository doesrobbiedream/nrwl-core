import { CallHandler, ExecutionContext, Inject, mixin, NestInterceptor, Type } from '@nestjs/common'
import { S3FileUploadService } from './s3-file-upload.service'
import { Request, Response } from 'express'

export function S3FileInterceptor(
  bucket: string,
  fieldName: string = 'file'
): Type<NestInterceptor> {
  class MixinInterceptor implements NestInterceptor {
    constructor(@Inject(S3FileUploadService) protected s3Svc: S3FileUploadService) {}

    async intercept(context: ExecutionContext, next: CallHandler) {
      const ctx = context.switchToHttp()
      const req: Request = ctx.getRequest()
      const res: Response = ctx.getResponse()
      await new Promise<void>((resolve, reject) => {
        this.s3Svc.uploader(bucket)?.single(fieldName)(req, res, (error: any) => {
          if (error) {
            reject(error)
          }
          resolve()
        })
      })
      return next.handle()
    }
  }

  const Interceptor = mixin(MixinInterceptor)
  return Interceptor as Type<NestInterceptor>
}
