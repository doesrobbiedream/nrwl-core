import { S3 } from 'aws-sdk'

export namespace FileUploadModuleValues {
  export const INSTANCE_OPTIONS = 'INSTANCE_OPTIONS'

  export interface ModuleOptionsInterface {
    buckets: MulterS3BucketOptions[]
    S3Options: S3.Types.ClientConfiguration
  }

  export interface AsyncModuleOptionsInterface {
    useFactory?: (...args: any[]) => Promise<ModuleOptionsInterface> | ModuleOptionsInterface
    inject?: any[]
    imports?: any[]
  }

  export interface MulterS3BucketOptions {
    bucket: string
    s3?: S3
    acl?: string
    cacheControl?:
      | ((
          req: Express.Request,
          file: Express.Multer.File,
          callback: (error: any, cacheControl?: string) => void
        ) => void)
      | string
    serverSideEncryption?:
      | ((
          req: Express.Request,
          file: Express.Multer.File,
          callback: (error: any, serverSideEncryption?: string) => void
        ) => void)
      | string
    contentDisposition?:
      | ((
          req: Express.Request,
          file: Express.Multer.File,
          callback: (error: any, contentDisposition?: string) => void
        ) => void)
      | string

    key?(
      req: Express.Request,
      file: Express.Multer.File,
      callback: (error: any, key?: string) => void
    ): void

    contentType?(
      req: Express.Request,
      file: Express.Multer.File,
      callback: (error: any, mime?: string, stream?: NodeJS.ReadableStream) => void
    ): void

    metadata?(
      req: Express.Request,
      file: Express.Multer.File,
      callback: (error: any, metadata?: any) => void
    ): void
  }
}
