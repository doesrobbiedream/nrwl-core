import { TransformFnParams } from 'class-transformer'

export namespace MongooseUtils {
  export const IDTransform = (params: TransformFnParams) => params.obj._id
}
