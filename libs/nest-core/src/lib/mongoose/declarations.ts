import { FilterQuery, Query, QueryOptions } from 'mongoose'

export namespace MongooseDeclarations {
  export interface FindQuery<T> {
    condition: FilterQuery<T>
    projection?: any | null
    options?: QueryOptions
    callback?: (err: any, res: T[]) => void
  }

  export interface MongoRepositoryInterface<T> {
    create(data: T | any): Promise<T>

    findOneById(id: string): Query<T, any> | null

    findByCondition(filterCondition: any): Query<T[], any> | null

    findAll(): Query<T[], any> | null

    remove(id: string): Query<any, any> | null

    aggregate<R = any>(aggregateConditions: any[]): Promise<Array<R>>
  }
}
