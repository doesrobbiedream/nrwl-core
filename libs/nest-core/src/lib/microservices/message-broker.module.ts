import { DynamicModule, Module } from '@nestjs/common'
import { MicroserviceMessagingDeclarations } from './microservices-messaging.declarations'
import { ClientOptions } from '@nestjs/microservices'
import { MessageBrokerProxyService } from './message-broker-proxy.service'
import MESSAGE_BROKER_TOKEN = MicroserviceMessagingDeclarations.MESSAGE_BROKER_TOKEN

@Module({})
export class MessageBrokerModule {
  public static forRoot(configuration: ClientOptions): DynamicModule {
    return {
      global: true,
      module: MessageBrokerModule,
      providers: [
        {
          provide: MESSAGE_BROKER_TOKEN,
          useFactory: () => new MessageBrokerProxyService(configuration)
        }
      ],
      exports: [MESSAGE_BROKER_TOKEN]
    }
  }

  public static forRootAsync(configuration: {
    factory: (...args: any[]) => MessageBrokerProxyService
    inject: any[]
    imports: any[]
  }): DynamicModule {
    return {
      global: true,
      module: MessageBrokerModule,
      providers: [
        {
          provide: MESSAGE_BROKER_TOKEN,
          useFactory: configuration.factory,
          inject: [...configuration.inject]
        }
      ],
      imports: [...configuration.imports],
      exports: [MESSAGE_BROKER_TOKEN]
    }
  }
}
