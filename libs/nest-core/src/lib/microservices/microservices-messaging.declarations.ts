import { Observable } from 'rxjs'
import {
  ClassProvider,
  ExistingProvider,
  FactoryProvider,
  ValueProvider
} from '@nestjs/common/interfaces/modules/provider.interface'
import { MessageBrokerProxyService } from './message-broker-proxy.service'
import { Exception } from '../exception-handling/exceptions/exception'

export namespace MicroserviceMessagingDeclarations {
  export const MESSAGE_BROKER_TOKEN = 'MESSAGE_BROKER_TOKEN'

  export enum PROXY_TYPES {
    ORCHESTRATOR = 'ORCHESTRATOR',
    MESSAGE_BROKER = 'MESSAGE_BROKER'
  }

  export type Provider<T> =
    | ClassProvider<T>
    | ValueProvider<T>
    | FactoryProvider<T>
    | ExistingProvider<T>
  export type MessageFactory = (data?: any) => IMessage
  export type OrchestrationFactory = (input?: any) => IOrchestration
  export type IOrchestrationWithSteps = Record<string, any> & {
    id: string
    steps: any[]
    response: (...args) => any
  }
  export type BrokersListProviders = {
    [PROXY_TYPES.ORCHESTRATOR]?: Provider<OrchestrationBrokerProxy>
    [PROXY_TYPES.MESSAGE_BROKER]?: Provider<MessageBrokerProxyService>
  }
  export type BrokersList = {
    [PROXY_TYPES.ORCHESTRATOR]?: OrchestrationBrokerProxy
    [PROXY_TYPES.MESSAGE_BROKER]?: MessageBrokerProxyService
  }

  export interface IMessage {
    type: 'send' | 'emit'
    pattern: string
  }

  export interface IOrchestration {
    type: 'procedure' | 'composition' | 'transaction'
    orchestration: IOrchestrationWithSteps

    setId(id: string): IOrchestration

    addStep(step: any): IOrchestration

    setResponse(response: (...args) => any): IOrchestration

    build(): any
  }

  export interface OrchestrationBrokerProxy {
    procedure: (procedure: any, input: any) => Observable<any>
    composition: (composition: any, input: any) => Observable<any>
    transaction: (transaction: any, input: any) => Observable<any>
  }

  export interface MessageBrokerProxy {
    send(pattern: string, data: any): Observable<any>

    emit(pattern: string, data: any): Observable<any>
  }

  export class Message implements IMessage {
    constructor(public type: 'send' | 'emit', public pattern: string) {}
  }

  export class Orchestration implements IOrchestration {
    public orchestration: IOrchestrationWithSteps = {
      id: null,
      steps: [],
      response: null
    }

    constructor(public type: 'procedure' | 'composition' | 'transaction') {}

    public setId(id: string) {
      this.orchestration.id = id
      return this
    }

    public addStep(step: any) {
      this.orchestration.steps.push(step)
      return this
    }

    public setResponse(response: (...args) => any) {
      this.orchestration.response = response
      return this
    }

    public build() {
      return this.orchestration
    }
  }

  export function executionHandlerType(execution: Message | Orchestration) {
    switch (execution.constructor) {
      case Message:
        return PROXY_TYPES.MESSAGE_BROKER
      case Orchestration:
        return PROXY_TYPES.ORCHESTRATOR
      default:
        throw new Exception({
          status: 500,
          exception: 'INVALID_ORCHESTRATION_TYPE',
          message: 'Internal server error'
        })
    }
  }

  export function submitToHandler(
    brokers: BrokersList,
    execution: Message | Orchestration,
    input: any
  ): Observable<any> {
    switch (executionHandlerType(execution)) {
      case PROXY_TYPES.ORCHESTRATOR:
        return brokers[PROXY_TYPES.ORCHESTRATOR][execution.type](
          (execution as Orchestration).build(),
          input
        )
      case PROXY_TYPES.MESSAGE_BROKER:
        return brokers[PROXY_TYPES.MESSAGE_BROKER].client[execution.type](
          (execution as Message).pattern,
          input
        )
    }
  }
}
