export interface ResponseTransformerOptions {
  className?: new () => any
  classNamesList?: { [key: string]: new () => any }
  request_param_key?: string
}
