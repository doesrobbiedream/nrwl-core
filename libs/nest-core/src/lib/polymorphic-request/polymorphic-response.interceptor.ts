import { CallHandler, ExecutionContext, NestInterceptor } from '@nestjs/common'
import { Observable } from 'rxjs'

import { PolymorphicResponseInterceptorOptions } from './polymorphic-transformer-interceptor-options.interface'
import { map } from 'rxjs/operators'
import { plainToClass } from 'class-transformer'

export class PolymorphicResponseInterceptor implements NestInterceptor {
  constructor(protected transformerOptions: PolymorphicResponseInterceptorOptions) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const req = context.switchToHttp().getRequest()
    return next
      .handle()
      .pipe(
        map((response) =>
          plainToClass(
            this.transformerOptions.transformClasses[req.polymorphic_discriminator],
            response,
            { excludeExtraneousValues: true }
          )
        )
      )
  }
}
