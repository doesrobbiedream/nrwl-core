import { APP_FILTER } from '@nestjs/core'
import { ExceptionFilter } from './filters/exception-filter'
import { Provider } from '@nestjs/common'
import { ExceptionHandlingDictionaries } from './dictionaries'

export const EXCEPTION_FILTER_PROVIDER: (
  dictionary?: ExceptionHandlingDictionaries.ErrorMessageDictionary
) => Provider<ExceptionFilter> = (
  dictionary?: ExceptionHandlingDictionaries.ErrorMessageDictionary
) => ({
  provide: APP_FILTER,
  useFactory: () => new ExceptionFilter(dictionary)
})
