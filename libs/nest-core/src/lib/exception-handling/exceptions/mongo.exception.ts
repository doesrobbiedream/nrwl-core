import { RegisteredException } from './registered.exception'
import { MongoError } from 'mongodb'

export class MongoException extends RegisteredException {
  public exception: string

  constructor(public error: MongoError, public data?: Record<string, any>) {
    super()
    this.exception = `MONGO_${error.code}`
  }
}
