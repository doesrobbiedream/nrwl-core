import { ValidationError } from '@nestjs/common'
import { ExceptionHandlingResponses } from '../responses'
import { ExceptionHandlingDictionaries } from '../dictionaries'
import ExceptionResponse = ExceptionHandlingResponses.ExceptionResponse
import ErrorMessageDeclaration = ExceptionHandlingDictionaries.ErrorMessageDeclaration

export class DTOValidationException implements ExceptionResponse {
  exception: string
  status: number
  timestamp: string
  request_path: string
  data: ExceptionHandlingResponses.ValidationErrorData

  constructor(errors: ValidationError[]) {
    this.status = 400
    this.exception = 'DTO_VALIDATION_ERROR'
    this.timestamp = new Date().toISOString()
    this.request_path = ''
    this.data = {
      errors: errors.map((e) => this.parseError(e)),
      input: errors.map((e) => e.target),
      message: "Incoming data hasn't passed validation. Please check your inputs and try again"
    }
  }

  public overwrite(declaration?: ErrorMessageDeclaration) {
    if (declaration) {
      this.status = declaration.status
      this.exception = declaration.exception
      this.data = { ...this.data, message: declaration.message }
    }
  }

  protected parseError: (e: ValidationError) => ExceptionHandlingResponses.ValidationError = (
    e: ValidationError
  ) => ({
    property: e.property,
    value: (!e.children && e.value) || '',
    children: e.children && e.children?.map((x) => this.parseError(x)),
    constraints: (!e.children && e.constraints) || undefined
  })
}
