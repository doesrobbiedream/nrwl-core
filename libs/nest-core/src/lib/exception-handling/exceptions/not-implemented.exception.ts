import { RegisteredException } from './registered.exception'

export class NotImplementedException extends RegisteredException {
  exception = 'NOT_IMPLEMENTED_METHOD'

  constructor(public data?: Record<string, any> | undefined) {
    super()
  }
}
