export abstract class RegisteredException {
  abstract exception: string
  abstract data?: Record<string, any>
  error?: any
}
