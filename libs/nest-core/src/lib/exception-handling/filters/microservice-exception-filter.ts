import {
  ArgumentsHost,
  Catch,
  ExceptionFilter as NestExceptionFilter,
  Logger
} from '@nestjs/common'
import { Exception } from '../exceptions/exception'
import { AbstractExceptionFilter } from './abstract.exception-filter'
import { throwError } from 'rxjs'

@Catch()
export class MicroserviceExceptionFilter
  extends AbstractExceptionFilter
  implements NestExceptionFilter {
  catch(exception: any, host: ArgumentsHost): any {
    if (process.env.NODE_ENV === 'development') {
      Logger.debug(JSON.stringify(exception))
    }
    const ctx = host.switchToRpc()
    const pattern = (ctx.getContext().args as Array<string>).join(' ')
    const Exception: Exception = this.parseException(exception)
    Exception.request_path = pattern
    Logger.error(
      `${Exception.status} | ${Exception.exception} | ${Exception.request_path} | ${Exception.timestamp}`
    )
    return throwError(Exception)
  }
}
