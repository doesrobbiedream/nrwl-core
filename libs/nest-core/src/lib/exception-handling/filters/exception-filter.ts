import {
  ArgumentsHost,
  Catch,
  ExceptionFilter as NestExceptionFilter,
  Logger
} from '@nestjs/common'
import { Request, Response } from 'express'
import { AbstractExceptionFilter } from './abstract.exception-filter'
import { Exception } from '../exceptions/exception'

@Catch()
export class ExceptionFilter extends AbstractExceptionFilter implements NestExceptionFilter {
  catch(exception: any, host: ArgumentsHost): any {
    if (process.env.NODE_ENV === 'development') {
      Logger.debug(JSON.stringify(exception))
    }
    const ctx = host.switchToHttp()
    const response = ctx.getResponse<Response>()
    const request = ctx.getRequest<Request>()
    const Exception: Exception = this.parseException(exception)
    Exception.request_path = request.url
    Logger.error(
      `${request.method} | ${Exception.status} | ${Exception.exception} | ${Exception.request_path} | ${Exception.timestamp}`
    )
    response.status(Exception.status).json(Exception)
  }
}
