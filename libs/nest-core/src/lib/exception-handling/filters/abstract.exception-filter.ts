import { ExceptionHandlingDictionaries } from '../dictionaries'
import _merge from 'lodash/merge'
import { getBaseClassFunction } from '@doesrobbiedream/ts-utils'
import { RegisteredException } from '../exceptions/registered.exception'
import { Exception } from '../exceptions/exception'
import { DTOValidationException } from '../exceptions/dto-validation.exception'
import { FromRPCException } from '../exceptions/from-rpc.exception'
import DefaultDictionary = ExceptionHandlingDictionaries.DefaultDictionary
import ErrorMessageDictionary = ExceptionHandlingDictionaries.ErrorMessageDictionary

export abstract class AbstractExceptionFilter {
  protected dictionary: ErrorMessageDictionary = DefaultDictionary

  constructor(dictionaries?: ErrorMessageDictionary) {
    this.dictionary = _merge(this.dictionary, dictionaries)
  }

  protected parseException(exception: any) {
    switch (getBaseClassFunction(exception)) {
      case RegisteredException:
        return new Exception(
          this.dictionary[exception.exception] || this.dictionary['DEFAULT'],
          exception.data
        )
      case DTOValidationException:
      case FromRPCException:
        exception.overwrite(this.dictionary[exception.exception])
        return exception
      default:
        return new Exception(
          this.dictionary[
            (exception.getStatus && exception.getStatus()) || exception.status || 'DEFAULT'
          ],
          exception.data
        )
    }
  }
}
