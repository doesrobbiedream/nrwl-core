## @doesrobbiedream/ng-core [1.0.1](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/ng-core@1.0.0...@doesrobbiedream/ng-core@1.0.1) (2021-06-29)





### Dependencies

* **@doesrobbiedream/themify:** upgraded to 1.1.0

# @doesrobbiedream/ng-core 1.0.0 (2021-06-17)


### Features

* **master:** automated library publishing ([b85d932](https://gitlab.com/doesrobbiedream/nrwl-core/commit/b85d93299a57fad10f0de27c35b27e547f93c657))





### Dependencies

* **@doesrobbiedream/themify:** upgraded to 1.0.0
