// eslint-disable-file
import { ControlValueAccessor } from '@angular/forms'
import { Component, ElementRef, EventEmitter, Input, Output } from '@angular/core'

export interface ControlValueAccessorInterface extends ControlValueAccessor {
  disabled: boolean
  disabledChanged: EventEmitter<boolean>
  valueChanged: EventEmitter<any>
  value: any

  notifyValueChange(value: any): void
}

export function ControlValueAccessorMixin(BaseClass = class {} as any): any {
  @Component({ selector: 'drd-control-value-accessor-mixin', template: '' })
  // eslint-disable-next-line
  class Temporary extends BaseClass implements ControlValueAccessorInterface {
    @Input() disabled = false
    @Output() disabledChanged: EventEmitter<boolean> = new EventEmitter()
    @Output() valueChanged: EventEmitter<any> = new EventEmitter()
    onChange!: (value: any) => any
    onTouched!: () => any
    protected valueContainer: any
    protected _el!: ElementRef

    get value(): any {
      return this.valueContainer
    }

    set value(value: any) {
      this.valueContainer = value
      this.notifyValueChange(value)
    }

    public notifyValueChange(value: any): void {
      this.valueChanged.emit(value)
      if (this.onChange) {
        this.onChange(value)
      }
    }

    public writeValue(value: any): void {
      if (value === undefined || value === null) {
        this.value = ''
        return
      }
      this.value = value
    }

    public registerOnChange(fn: any): void {
      this.onChange = fn
    }

    public registerOnTouched(fn: any): void {
      this.onTouched = fn
    }

    public setDisabledState?(isDisabled: boolean): void {
      this.disabled = isDisabled
      this.disabledChanged.emit(isDisabled)
    }
  }

  return Temporary
}
