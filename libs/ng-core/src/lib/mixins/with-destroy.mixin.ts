import { Subject } from 'rxjs'

export interface ComponentWithDestroyInterface {
  onDestroy$: Subject<any>

  ngOnDestroy(): void
}

export function ComponentWithDestroy(BaseClass = class {} as any) {
  return class extends BaseClass {
    onDestroy$: Subject<any> = new Subject()

    ngOnDestroy() {
      this.onDestroy$.next()
      this.onDestroy$.complete()
    }
  }
}
