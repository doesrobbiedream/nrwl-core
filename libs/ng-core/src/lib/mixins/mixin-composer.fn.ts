import { Constructor } from './constructor.type'
import _head from 'lodash/head'
import _tail from 'lodash/tail'

export function Mixin<T>(
  functions: ((...args: any) => Constructor<any>) | Array<(args?: any) => Constructor<any>> | any,
  baseClass?: any
): Constructor<T> {
  functions = Array.isArray(functions) ? functions : [functions]
  return _tail(functions).length > 0
    ? _head(<any[]>functions)(Mixin(_tail(functions)))
    : _head(<any[]>functions)(baseClass)
}
