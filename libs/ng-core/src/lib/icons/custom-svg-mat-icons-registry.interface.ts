import { InjectionToken } from '@angular/core'

export namespace CustomMatIconRegistry {
  export const ICONS = new InjectionToken('CUSTOM_ICONS')

  export interface Registry {
    namespace: string
    icons: SvgIconDefinition[]
  }

  export interface SvgIconDefinition {
    name: string
    path: string
    namespace?: string
  }
}
