import { Inject, NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { MatIconModule, MatIconRegistry } from '@angular/material/icon'
import { DomSanitizer } from '@angular/platform-browser'

import _forEach from 'lodash/forEach'
import { CustomMatIconRegistry } from './custom-svg-mat-icons-registry.interface'
import ICONS = CustomMatIconRegistry.ICONS
import Registry = CustomMatIconRegistry.Registry

@NgModule({
  declarations: [],
  imports: [CommonModule, MatIconModule]
})
export class CustomIconsRegistryModule {
  constructor(
    protected registry: MatIconRegistry,
    domSanitizer: DomSanitizer,
    @Inject(ICONS) custom_registry: Registry
  ) {
    _forEach(custom_registry.icons, (values: CustomMatIconRegistry.SvgIconDefinition) =>
      registry.addSvgIcon(values.name, domSanitizer.bypassSecurityTrustResourceUrl(values.path))
    )
  }
}
