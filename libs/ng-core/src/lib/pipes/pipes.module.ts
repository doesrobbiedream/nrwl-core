import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { BackgroundUrlPipe } from './background-url.pipe'
import { ArrayFirstPipe } from './array-first.pipe'
import { LoadingDotsPipe } from './loading-dots.pipe'
import { SafeCssPipe } from './safe-css.pipe'
import { SafeHtmlPipe } from './safe-html.pipe'
import { ToArrayPipe } from './to-array.pipe'

@NgModule({
  declarations: [
    BackgroundUrlPipe,
    ArrayFirstPipe,
    LoadingDotsPipe,
    SafeCssPipe,
    SafeHtmlPipe,
    ToArrayPipe
  ],
  imports: [CommonModule],
  exports: [
    BackgroundUrlPipe,
    ArrayFirstPipe,
    LoadingDotsPipe,
    SafeCssPipe,
    SafeHtmlPipe,
    ToArrayPipe
  ]
})
export class PipesModule {}
