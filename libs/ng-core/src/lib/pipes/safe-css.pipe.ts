import { Pipe, PipeTransform } from '@angular/core'
import { DomSanitizer, SafeStyle } from '@angular/platform-browser'
import DOMPurify from 'dompurify'

@Pipe({
  name: 'safeCss',
  pure: true
})
export class SafeCssPipe implements PipeTransform {
  constructor(protected sanitizer: DomSanitizer) {}

  public transform(value: string | Node): SafeStyle {
    const sanitizedContent = DOMPurify.sanitize(value)
    return this.sanitizer.bypassSecurityTrustStyle(sanitizedContent)
  }
}
