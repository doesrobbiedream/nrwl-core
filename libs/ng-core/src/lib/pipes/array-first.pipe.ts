import { Pipe, PipeTransform } from '@angular/core'
import _head from 'lodash/head'

@Pipe({
  name: 'arrayFirst'
})
export class ArrayFirstPipe implements PipeTransform {
  transform<T>(value: T[]): T | null {
    return (value && _head(value)) || null
  }
}
