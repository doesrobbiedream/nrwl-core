import { AnimationTriggerMetadata, transition, trigger } from '@angular/animations'

export function BlockInitialRenderAnimation(): AnimationTriggerMetadata {
  return trigger('blockInitialRenderAnimation', [transition(':enter', [])])
}
