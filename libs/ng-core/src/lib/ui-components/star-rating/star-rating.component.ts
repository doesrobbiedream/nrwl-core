import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostListener,
  Input,
  OnInit
} from '@angular/core'

@Component({
  selector: 'drd-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StarRatingComponent implements OnInit {
  @Input() rating = 0
  @Input() width = '16px'
  @Input() baseFill = 'lightgrey'
  @Input() checkedFill = 'yellow'
  @Input() readonly: boolean | undefined
  public stars: Array<[boolean, boolean]> = []

  constructor(protected cdr: ChangeDetectorRef) {}

  @Input() set length(l: number) {
    this.stars = new Array(Number(l)).fill([]).map(() => [false, false])
    this.cdr.markForCheck()
  }

  @HostListener('click', ['$event'])
  click(event: MouseEvent) {
    event.stopPropagation()
  }

  @HostListener('mouseleave')
  rollbackRateToLockedValue() {
    this.updateStars(this.rating)
  }

  ngOnInit(): void {
    this.updateStars(this.rating)
  }

  rate(rate: number) {
    if (this.readonly) {
      return
    }
    this.rating = rate
    this.updateStars(rate)
  }

  showPretendedRate(rate: number, event: MouseEvent) {
    event.stopImmediatePropagation()
    if (this.readonly) {
      return
    }
    this.updateStars(rate)
  }

  color(checked: boolean) {
    return (checked && this.checkedFill) || this.baseFill
  }

  protected updateStars(rate: number) {
    this.stars.forEach((star, index) => {
      star[0] = rate - index > 0
      star[1] = rate - index > 0.5
    })
    this.cdr.detectChanges()
  }
}
