import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { DropFilesComponent } from './drop-files/drop-files.component'
import { NgxFileDropModule } from 'ngx-file-drop'
import { MatButtonModule } from '@angular/material/button'
import { ImagePreviewComponent } from './image-preview/image-preview.component'
import { ImageFileParserService } from './image-file-parser.service'
import { ImagePreviewPipe } from './image-preview.pipe'
import { FileUploadButtonDirective } from './file-upload-button.directive'

@NgModule({
  declarations: [
    DropFilesComponent,
    ImagePreviewComponent,
    ImagePreviewPipe,
    FileUploadButtonDirective
  ],
  exports: [DropFilesComponent, ImagePreviewComponent, ImagePreviewPipe, FileUploadButtonDirective],
  imports: [CommonModule, NgxFileDropModule, MatButtonModule],
  providers: [ImageFileParserService]
})
export class FileUploadModule {}
