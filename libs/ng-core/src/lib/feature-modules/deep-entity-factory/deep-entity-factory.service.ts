import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { DeepEntityFactoryModels } from './deep-entity-factory.models'
import { concat, forkJoin, Observable, of, Subject } from 'rxjs'
import { map, switchMap, tap, toArray } from 'rxjs/operators'
import _forEach from 'lodash/forEach'
import EntityMap = DeepEntityFactoryModels.EntityMap
import CarryOn = DeepEntityFactoryModels.CarryOn
import DEPENDENCY_MODE = DeepEntityFactoryModels.DEPENDENCY_MODE
import PropertyDefinition = DeepEntityFactoryModels.PropertyDefinition
import RootPropertyDefinition = DeepEntityFactoryModels.RootPropertyDefinition

@Injectable({
  providedIn: 'root'
})
export class DeepEntityFactoryService {
  constructor(protected http: HttpClient) {}

  public runFactoryWithMultiple<T>(
    input_data: T[],
    entities: EntityMap,
    root: RootPropertyDefinition
  ) {
    const carry = { entities_map: entities, created: {} }
    return concat(
      ...input_data.map((x) =>
        of(x).pipe(switchMap((i) => this.runFactory(i, entities, root, carry)))
      )
    ).pipe(toArray())
  }

  public runFactory<T>(
    input_data: T,
    entities: EntityMap,
    root: RootPropertyDefinition,
    carry_on?: CarryOn
  ): Observable<any> {
    if (!carry_on) {
      carry_on = {
        entities_map: entities,
        created: {}
      }
    }
    return this.createRelationalDependency(input_data, root as PropertyDefinition, carry_on)
  }

  public createDependencies(
    input_data: any,
    property: PropertyDefinition,
    carry_on: CarryOn
  ): Observable<any> {
    const dependenciesRecord: Record<string, Observable<any>> = {}
    _forEach(property.dependencies, (dependency, name) => {
      if (input_data[dependency.named || name]) {
        switch (dependency.mode) {
          case DEPENDENCY_MODE.RELATIONAL:
            dependenciesRecord[dependency.named || name] = this.createRelationalDependency(
              input_data[dependency.named || name],
              dependency,
              carry_on
            )
            break
          case DEPENDENCY_MODE.COLLECTION:
            dependenciesRecord[
              dependency.named || name
            ] = this.createRelationalDependencyCollection(
              input_data[dependency.named || name],
              dependency,
              carry_on
            )
            break
          case DEPENDENCY_MODE.EMBED:
            dependenciesRecord[dependency.named || name] = this.createEmbedWithDependencies(
              input_data[dependency.named || name],
              dependency,
              carry_on
            )
            break
          case DEPENDENCY_MODE.EMBED_COLLECTION:
            dependenciesRecord[
              dependency.named || name
            ] = this.createEmbedWithDependenciesCollection(
              input_data[dependency.named || name],
              dependency,
              carry_on
            )
            break
        }
      }
    })
    if (Object.keys(dependenciesRecord).length) {
      return forkJoin(dependenciesRecord)
    } else {
      return of(dependenciesRecord)
    }
  }

  public createEmbedWithDependencies(
    input_data: any,
    property: PropertyDefinition,
    carry_on: CarryOn
  ): Observable<any> {
    return this.createDependencies(input_data, property, carry_on).pipe(
      map((dependencies) => ({ ...input_data, ...dependencies }))
    )
  }

  public createEmbedWithDependenciesCollection(
    input_data: any[],
    property: PropertyDefinition,
    carry_on: CarryOn
  ): Observable<any[]> {
    return forkJoin(
      input_data.map((data) => this.createEmbedWithDependencies(data, property, carry_on))
    )
  }

  public createRelationalDependency(
    input_data: any,
    property: PropertyDefinition,
    carry_on: CarryOn
  ): Observable<any> {
    if (property.dependencies) {
      return this.createDependencies(input_data, property, carry_on).pipe(
        map((dependencies: any) => ({ ...input_data, ...dependencies })),
        switchMap((final_data) => this.create(final_data, property, carry_on))
      )
    } else {
      return this.create(input_data, property, carry_on)
    }
  }

  public createRelationalDependencyCollection(
    input_data: any[],
    property: PropertyDefinition,
    carry_on: CarryOn
  ): Observable<any[]> {
    return forkJoin(
      input_data.map((data) => this.createRelationalDependency(data, property, carry_on))
    )
  }

  public create(input_data: any, property: PropertyDefinition, carry_on: CarryOn): Observable<any> {
    const custom_id_path = carry_on.entities_map[property.entity].id_path
    const _id = (custom_id_path && input_data[custom_id_path]) || input_data.id || input_data._id
    const existing = (carry_on.created[property.entity] || {})[_id]
    const identity = (r: any) => r
    if (existing) {
      return of(existing).pipe(map((result) => (property.response_mapper || identity)(result)))
    } else {
      const s = new Subject()
      this.http
        .post(
          carry_on.entities_map[property.entity].endpoint,
          (property.input_mapper || identity)(input_data)
        )
        .pipe(
          tap((response) => {
            if (_id) {
              carry_on.created[property.entity] = carry_on.created[property.entity] || {}
              carry_on.created[property.entity][_id] = response
            }
          }),
          map((result) => (property.response_mapper || identity)(result))
        )
        .subscribe((response) => {
          s.next(response)
          s.complete()
        })
      return s
    }
  }
}
