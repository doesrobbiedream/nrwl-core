import { DeepEntityFactoryModels } from './deep-entity-factory.models'
import DEPENDENCY_MODE = DeepEntityFactoryModels.DEPENDENCY_MODE

export namespace DeepEntityFactoryMocks {
  import EntityMap = DeepEntityFactoryModels.EntityMap
  import RootProperty = DeepEntityFactoryModels.RootPropertyDefinition
  import PropertyDefinition = DeepEntityFactoryModels.PropertyDefinition
  import CarryOn = DeepEntityFactoryModels.CarryOn

  export namespace UnitTests {
    export namespace create {
      export const input: any = {
        name: 'Homer'
      }
      export const property: PropertyDefinition = {
        entity: 'user',
        mode: DEPENDENCY_MODE.ROOT
      }
      export const carry_on: () => CarryOn = () => ({
        entities_map: {
          user: { endpoint: '/user' }
        },
        created: {}
      })
    }
    export namespace createWithResponseMapper {}
  }
  export namespace TwoLevelsDeep {
    export const Data = {
      name: 'Homer',
      address: {
        street: 'Evergreen Terrace',
        number: '742'
      }
    }
    export const Entities: EntityMap = {
      user: {
        endpoint: '/user'
      },
      address: {
        endpoint: '/address'
      }
    }
    export const root: RootProperty = {
      entity: 'user',
      dependencies: {
        address: {
          entity: 'address',
          mode: DEPENDENCY_MODE.RELATIONAL
        }
      }
    }
  }
  export namespace TwoLevelsDeepWithCollection {
    export const Data = [
      {
        user: 'Homer',
        categories: [
          {
            _id: 'some_crazy_id',
            slug: 'category_1'
          }
        ]
      },
      {
        user: 'Marge',
        categories: [
          {
            _id: 'some_crazy_id',
            slug: 'category_that_wont_be_created'
          }
        ]
      }
    ]
    export const Entities: EntityMap = {
      user: {
        endpoint: '/user'
      },
      category: {
        endpoint: '/category'
      }
    }
    export const root: RootProperty = {
      entity: 'user',
      dependencies: {
        categories: {
          entity: 'category',
          mode: DEPENDENCY_MODE.COLLECTION
        }
      }
    }
  }
  export namespace ThreeLevelsDeepCollectionWithRepeatedInCollection {
    export const Data = [
      {
        name: 'Homer',
        location: {
          address: {
            code: '00001',
            streets: [
              { id: 1, name: 'Street 1' },
              { id: 2, name: 'Street 2' },
              { id: 3, name: 'Street 3' }
            ]
          }
        }
      },
      {
        name: 'Marge',
        location: {
          address: {
            code: '00002',
            streets: [
              { id: 1, name: 'Street 1' },
              { id: 2, name: 'Street 2' },
              { id: 3, name: 'Street 3' },
              { id: 4, name: 'Street 4' }
            ]
          }
        }
      },
      {
        name: 'Apu',
        location: {
          address: {
            code: '00002',
            streets: [
              { id: 1, name: 'Street 1' },
              { id: 2, name: 'Street 2' },
              { id: 3, name: 'Street 3' },
              { id: 4, name: 'Street 4' }
            ]
          }
        }
      }
    ]
    export const Entities: EntityMap = {
      user: { endpoint: '/user' },
      location: { endpoint: '/location' },
      address: { endpoint: '/address', id_path: 'code' },
      street: { endpoint: '/street' }
    }
    export const root: RootProperty = {
      entity: 'user',
      dependencies: {
        location: {
          entity: 'location',
          mode: DEPENDENCY_MODE.EMBED,
          dependencies: {
            address: {
              entity: 'address',
              mode: DEPENDENCY_MODE.RELATIONAL,
              dependencies: {
                streets: {
                  entity: 'street',
                  mode: DEPENDENCY_MODE.COLLECTION
                }
              }
            }
          }
        }
      }
    }
  }
}
