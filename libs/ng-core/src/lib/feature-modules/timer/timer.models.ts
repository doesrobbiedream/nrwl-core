export namespace TimerModels {
  export interface TimerOptions {
    interval?: number
  }

  export const TimerDefaults: TimerOptions = {
    interval: 100
  }

  export interface TimerValues {
    emissions: number
    time: number
  }
}
