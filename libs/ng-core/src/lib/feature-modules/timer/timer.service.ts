import { Injectable } from '@angular/core'
import { TimersSession } from './timers-session'
import { Observable } from 'rxjs'
import _omit from 'lodash/omit'

@Injectable({
  providedIn: 'root'
})
export class TimerService {
  protected sessions: { [key: string]: TimersSession } = {}

  createSession(destroyer$: Observable<any>) {
    const session = new TimersSession(destroyer$)
    this.sessions[session.id] = session
    const destroyed = destroyer$.subscribe(() => {
      this.removeSession(session.id)
      destroyed.unsubscribe()
    })
    return session
  }

  removeSession(id: string) {
    ;(this.sessions[id] as TimersSession)?.destroy()
    this.sessions = _omit(this.sessions, id)
  }
}
