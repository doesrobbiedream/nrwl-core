import { Timer } from './timer'
import { Subject } from 'rxjs'

describe('Timer class', () => {
  let timer: Timer
  const destroyer = new Subject()
  beforeEach(() => {
    timer = new Timer('test-timer', destroyer)
  })
  afterEach(() => {
    destroyer.next()
  })
  test('should create a new timer', () => {
    expect(timer).toBeTruthy()
  })
  test('should init the timer', (done) => {
    timer.init()
    setTimeout(() => {
      expect(timer.values.emissions).toBeGreaterThan(0)
      expect(timer.values.emissions).toBeLessThanOrEqual(5)
      done()
    }, 500)
  })
  test('should stop the timer', (done) => {
    timer.init()
    setTimeout(() => {
      timer.stop()
    }, 500)
    setTimeout(() => {
      expect(timer.values.emissions).toBeGreaterThan(0)
      expect(timer.values.emissions).toBeLessThanOrEqual(5)
      done()
    }, 1000)
  })
  test('should restart the timer', (done) => {
    timer.init()
    setTimeout(() => {
      timer.reset()
    }, 500)
    setTimeout(() => {
      expect(timer.values.emissions).toBeGreaterThan(0)
      expect(timer.values.emissions).toBeLessThanOrEqual(5)
      done()
    }, 1000)
  })
  test('should pause & unpause the timer', (done) => {
    timer.init()
    setTimeout(() => {
      timer.pause()
    }, 500)
    setTimeout(() => {
      timer.start()
    }, 1000)
    setTimeout(() => {
      expect(timer.values.emissions).toBeGreaterThan(5)
      expect(timer.values.emissions).toBeLessThanOrEqual(10)
      done()
    }, 1500)
  })
  test('should init, reset, pause & unpause without loosing consistency ', (done) => {
    timer.init()
    setTimeout(() => {
      expect(timer.values.emissions).toBeGreaterThanOrEqual(0)
      expect(timer.values.emissions).toBeLessThanOrEqual(5)
      timer.reset()
    }, 500)
    setTimeout(() => {
      expect(timer.values.emissions).toBeGreaterThanOrEqual(0)
      expect(timer.values.emissions).toBeLessThanOrEqual(5)
      timer.pause()
    }, 1000)
    setTimeout(() => {
      expect(timer.values.emissions).toBeGreaterThanOrEqual(0)
      expect(timer.values.emissions).toBeLessThanOrEqual(5)
      timer.start()
    }, 1500)
    setTimeout(() => {
      expect(timer.values.emissions).toBeGreaterThanOrEqual(5)
      expect(timer.values.emissions).toBeLessThanOrEqual(10)
      done()
    }, 2000)
  })
})
