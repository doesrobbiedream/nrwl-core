export function isHexColor(color: string) {
  return new RegExp(/^#(?:[0-9a-f]{3}){1,2}$/i).test(color)
}
