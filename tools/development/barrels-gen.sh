#!/bin/bash

root="$PWD"

libs=$(nx affected:libs --plain)

if [ "${#libs}" -gt "0" ]; then
    IFS=' ' read -r -a array <<< "$libs"
    for element in "${array[@]}"
    do
      echo "Creating Barrel for $element"
      barrelsby -D -q -S -d "$root"/libs/"$element"/src -e=".spec.ts$" -e="test-setup.ts"
    done;
fi
