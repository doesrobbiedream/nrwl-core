import {
  formatFiles,
  generateFiles,
  getProjects,
  Tree,
  readProjectConfiguration
} from '@nrwl/devkit'
import { SchemaInterface } from './schema.interface'

export default async function (host: Tree, schema: SchemaInterface) {
  if (schema.all) {
    const excluded = schema.exclude?.split(',') || []
    const projects = Array.from(getProjects(host).keys()).filter((name) => {
      return getProjects(host).get(name).projectType === 'library' && excluded.indexOf(name) === -1
    })
    projects.forEach((library) => CreateDeploymentFileForPackage(host, { ...schema, library }))
  } else if (schema.library) {
    CreateDeploymentFileForPackage(host, schema)
  } else {
    throw Error('No library was defined.')
  }
  await formatFiles(host)
}

function CreateDeploymentFileForPackage(host: Tree, schema: SchemaInterface) {
  const libraryRoot = readProjectConfiguration(host, schema.library)
  const rootFolder = libraryRoot.root
  const template_data = {
    library_name: schema.library
  }
  // GENERATE NEW FILES
  generateFiles(host, 'tools/generators/semantic-release-config/files', rootFolder, template_data)
}
