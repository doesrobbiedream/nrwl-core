######################################################################
FROM node:12.13-alpine as prod-dependencies
######################################################################
WORKDIR /app
COPY package*.json ./
RUN npm ci --production --ignore-scripts
######################################################################
FROM node:12.13-alpine as dev-dependencies
######################################################################
WORKDIR /app
COPY package*.json ./
RUN npm ci --ignore-scripts
######################################################################
FROM node:12.13-alpine as builder
######################################################################
WORKDIR /app
ARG APP
ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV
COPY --from=dev-dependencies /app /app
COPY apps/$APP apps/$APP
COPY libs libs
COPY angular.json nx.json tsconfig.base.json ./
RUN $(npm bin)/nx run $APP:build --prod
######################################################################
FROM node:12.13-alpine as final
######################################################################
WORKDIR /app
ARG APP
COPY --from=prod-dependencies /app  .
COPY --from=builder /app/dist/apps/$APP .

CMD ["node", "main.js"]
