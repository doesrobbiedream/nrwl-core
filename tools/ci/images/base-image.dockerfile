#### DOCKER IMAGE NAME >>> registry.gitlab.com/does-robbie-dream/development-projects/core/base-dc
FROM docker:19.03.12
RUN apk update && apk upgrade && apk add curl bash docker-compose git npm && npm i -g npm@latest && npm install -g @nrwl/cli
SHELL ["/bin/bash", "-c"]
